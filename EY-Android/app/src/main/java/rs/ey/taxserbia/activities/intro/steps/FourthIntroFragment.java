package rs.ey.taxserbia.activities.intro.steps;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.intro.IntroActivity;
import rs.ey.taxserbia.adapters.settings.IndustriesListAdapter;
import rs.ey.taxserbia.entities.settings.IndustryItem;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.services.LocalStorage;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

public class FourthIntroFragment extends Fragment implements Responsable {

    private FrameLayout cbAll;
    private ImageView imgCbAll;

    private NestedScrollView scrollView;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/news/categories";

    private RecyclerView industriesRecyclerView;
    private IndustriesListAdapter industriesListAdapter;
    private ArrayList<IndustryItem> industriesList;

    public FourthIntroFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fourth_intro, container, false);
        initComponents(view);

        if (CheckInternetConnection.isOnline(this.getContext())) {
            requestIndustriesList();
        } else {
            Toast.makeText(this.getContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }

        initCbStatuses();
        setOnClickListeners();

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                IntroActivity.gestureDetectorCompat.onTouchEvent(event);
                return false;
            }
        });

        industriesRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                IntroActivity.gestureDetectorCompat.onTouchEvent(event);
                return false;
            }
        });

        return view;
    }

    private void setOnClickListeners() {
        cbAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EyApplication.cbAllStatus = !EyApplication.cbAllStatus;
                if (EyApplication.cbAllStatus) {
                    selectAll();
                } else {
                    deselectAll();
                }
            }
        });
    }

    private void initComponents(View view) {
        cbAll = view.findViewById(R.id.cbAll);
        imgCbAll = view.findViewById(R.id.imgCbAll);

        scrollView = view.findViewById(R.id.intro_fourth_scroll);

        industriesRecyclerView = view.findViewById(R.id.choose_industries_recycler_view);
        httpService = new VolleyService(this, this.getContext());
    }

    private void initCbStatuses() {
        if (EyApplication.cbAllStatus) {
            imgCbAll.setVisibility(View.VISIBLE);
            cbAll.setBackgroundColor(getResources().getColor(R.color.intro_step_4_active_cb_bg));
        } else {
            imgCbAll.setVisibility(View.GONE);
            cbAll.setBackgroundColor(getResources().getColor(R.color.intro_step_4_inactive_cb_bg));
        }
    }

    private void deselectAll() {
        EyApplication.cbAllStatus = false;
        LocalStorage.setIsSelectedAll(this.getContext(), false);
        EyApplication.chosenIndustries.clear();
        imgCbAll.setVisibility(View.GONE);
        cbAll.setBackgroundColor(getResources().getColor(R.color.intro_step_4_inactive_cb_bg));
        industriesListAdapter.notifyItemRangeChanged(0, industriesList.size());
    }

    private void selectAll() {
        EyApplication.chosenIndustries.clear();
        EyApplication.chosenIndustries.addAll(industriesList);
        EyApplication.cbAllStatus = true;
        LocalStorage.setIsSelectedAll(this.getContext(), true);
        imgCbAll.setVisibility(View.VISIBLE);
        cbAll.setBackgroundColor(getResources().getColor(R.color.intro_step_4_active_cb_bg));
        industriesListAdapter.notifyDataSetChanged();
    }

    private void requestIndustriesList() {
        industriesList = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort
                + "&parent=4";
        httpService.get(HTTP_URL + params);
    }

    private void createIndustryItem(ArrayList<IndustryItem> industriesList, String id, String name) {
        IndustryItem industryItem = new IndustryItem(id, name);
        industriesList.add(industryItem);
    }

    private void createListLayout() {
        industriesRecyclerView.setHasFixedSize(true);
        industriesListAdapter = new IndustriesListAdapter(this.getContext(), industriesList, cbAll, imgCbAll);
        industriesRecyclerView.setAdapter(industriesListAdapter);

        GridLayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
        industriesRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            JSONArray industriesList = res.getJSONArray("entity");
            for (int i = 0; i < industriesList.length(); i++) {
                JSONObject industryItem = industriesList.getJSONObject(i);
                createIndustryItem(this.industriesList, industryItem.getString("id"), industryItem.getString("name"));
            }

            createListLayout();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        try {
            Toast.makeText(getContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
