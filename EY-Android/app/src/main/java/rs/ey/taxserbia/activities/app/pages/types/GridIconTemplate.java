package rs.ey.taxserbia.activities.app.pages.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorEarningsTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorServiceContractTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorYearTaxTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplateSecond;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.GridIconTemplateAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.GridIconItemTemplate;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.helpers.Utils;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_EARNINGS;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_SERVICE_CONTRACT;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_YEAR_TAX;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALENDAR_TEMPLATE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EVENT_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EXPANDABLE_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.GRID_ICON;
import static rs.ey.taxserbia.entities.pages.types.PagesType.INDUSTRY_OVERVIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.NEWS_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.PAGE_VIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.POLL_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.SWIPED_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.VIDEO_LIST;

@SuppressWarnings("ConstantConditions")
public class GridIconTemplate extends AppCompatActivity implements Responsable {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private ImageView imgCover;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/menu";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";

    private final static int REQUEST_TYPE_SEARCH = 2;
    private final static int REQUEST_TYPE_BASIC = 1;

    private NestedScrollView contentView;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    private RecyclerView gridIconRecyclerView;
    private ArrayList<GridIconItemTemplate> gridIconPages;
    private String id;
    private String title;

    private LinearLayout lastItemLayout;
    private ImageView imgLastItemIcon;
    private TextView txtLastItemTitle;
    private GridIconItemTemplate lastItemTemplate;
    private ImageView bgLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_grid_icon);

        id = getIntent().getExtras().getString("id");
        title = getIntent().getExtras().getString("title");

        gridIconRecyclerView = findViewById(R.id.grid_icon_recycler_view);

        StatusBarUtil.setColor(GridIconTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (CheckInternetConnection.isOnline(this)) {
            requestPagesList();   
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        lastItemLayout = findViewById(R.id.last_full_item);
        imgLastItemIcon = findViewById(R.id.img_last_item_icon);
        txtLastItemTitle = findViewById(R.id.txt_last_item_title);

        contentView = findViewById(R.id.content_view);
        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        contentView.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);

        bgLayout = findViewById(R.id.bg_layout);
        bgLayout.setVisibility(View.GONE);

        imgCover = findViewById(R.id.img_cover);
        httpService = new VolleyService(this, this, REQUEST_TYPE_BASIC);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GridIconTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(GridIconTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GridIconTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GridIconTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GridIconTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        lastItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (lastItemTemplate.getType()) {
                    case GRID_ICON:
                        intent = new Intent(GridIconTemplate.this, GridIconTemplate.class);
                        intent.putExtra("id", lastItemTemplate.getId());
                        startActivity(intent);
                        break;
                    case NEWS_LIST:
                        intent = new Intent(GridIconTemplate.this, NewsListTemplate.class);
                        intent.putExtra("id", lastItemTemplate.getObjectId());
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case EXPANDABLE_LIST:
                        intent = new Intent(GridIconTemplate.this, ExpandableListTemplate.class);
                        intent.putExtra("id", lastItemTemplate.getObjectId());
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case VIDEO_LIST:
                        intent = new Intent(GridIconTemplate.this, VideoListTemplate.class);
                        intent.putExtra("id", lastItemTemplate.getObjectId());
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case POLL_LIST:
                        intent = new Intent(GridIconTemplate.this, PollsListTemplate.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case EVENT_LIST:
                        intent = new Intent(GridIconTemplate.this, EYEventsTemplate.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case SWIPED_LIST:
                        intent = new Intent(GridIconTemplate.this, SwipedListTemplate.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case CALCULATOR_EARNINGS:
                        intent = new Intent(GridIconTemplate.this, CalculatorEarningsTemplate.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case CALCULATOR_SERVICE_CONTRACT:
                        intent = new Intent(GridIconTemplate.this, CalculatorServiceContractTemplate.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case CALCULATOR_YEAR_TAX:
                        intent = new Intent(GridIconTemplate.this, CalculatorYearTaxTemplate.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case PAGE_VIEW:
                        intent = new Intent(GridIconTemplate.this, OnlyImagesTemplate.class);
                        intent.putExtra("id", lastItemTemplate.getObjectId());
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case CALENDAR_TEMPLATE:
                        intent = new Intent(GridIconTemplate.this, CalendarTemplateSecond.class);
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                    case INDUSTRY_OVERVIEW:
                        intent = new Intent(GridIconTemplate.this, IndustryOverviewTemplate.class);
                        intent.putExtra("id", lastItemTemplate.getId());
                        intent.putExtra("title", lastItemTemplate.getTitle());
                        startActivity(intent);
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(GridIconTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void createSubPage(ArrayList<GridIconItemTemplate> subPagesList, String id, String objectId, String title, String iconUrl, String subPageType) {
        GridIconItemTemplate gridIconPage = new GridIconItemTemplate(id, objectId, title, iconUrl, subPageType);
        subPagesList.add(gridIconPage);
    }

    private void requestPagesList() {
        gridIconPages = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort
                + "&id=" + id;
        httpService.get(HTTP_URL + params);
    }

    private void createListLayout() {
        gridIconRecyclerView.setHasFixedSize(true);
        final GridIconTemplateAdapter subPagesAdapter = new GridIconTemplateAdapter(this, gridIconPages);
        gridIconRecyclerView.setAdapter(subPagesAdapter);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        gridIconRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_TYPE_BASIC) {
                JSONObject entity = res.getJSONObject("entity");
                String coverImgUrl = entity.getString("header");
                if (coverImgUrl == null || coverImgUrl.equals("null")) {
                    imgCover.setImageDrawable(getResources().getDrawable(R.drawable.aktuelnosti_cover));
                } else {
                    Picasso.with(this)
                            .load(coverImgUrl)
                            .error(getResources().getDrawable(R.drawable.aktuelnosti_cover))
                            .into(imgCover);
                }

                JSONArray pagesList = entity.getJSONArray("list");

                if (pagesList.length() % 2 == 0) {
                    for (int i = 0; i < pagesList.length(); i++) {
                        JSONObject page = pagesList.getJSONObject(i);
                        createSubPage(gridIconPages, page.getString("id"), page.getString("object_id"), page.getString("title"), page.getString("icon"), page.getString("type"));
                    }

                    lastItemLayout.setVisibility(View.GONE);
                } else {
                    for (int i = 0; i < (pagesList.length() - 1); i++) {
                        JSONObject page = pagesList.getJSONObject(i);
                        createSubPage(gridIconPages, page.getString("id"), page.getString("object_id"), page.getString("title"), page.getString("icon"), page.getString("type"));
                    }
                    JSONObject lastItem = pagesList.getJSONObject(pagesList.length() - 1);

                    lastItemTemplate = new GridIconItemTemplate(lastItem.getString("id"), lastItem.getString("object_id"), lastItem.getString("title"), lastItem.getString("icon"), lastItem.getString("type"));

                    txtLastItemTitle.setText(lastItemTemplate.getTitle());
                    String itemIconUrl = lastItemTemplate.getIconUrl();
                    Drawable defaultIcon = getResources().getDrawable(R.drawable.poreske_vesti_icon);

                    if (itemIconUrl == null || itemIconUrl.equals("null")) {
                        imgLastItemIcon.setImageDrawable(defaultIcon);
                    } else {
                        Utils.fetchSvg(this, itemIconUrl, imgLastItemIcon);
                    }
                    lastItemLayout.setVisibility(View.VISIBLE);
                }

                createListLayout();
            } else {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
                lastItemLayout.setVisibility(View.GONE);
            }

            contentView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
            bgLayout.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
