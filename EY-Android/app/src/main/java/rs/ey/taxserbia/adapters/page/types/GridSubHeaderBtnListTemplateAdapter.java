package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.EYEventsTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.GridIconTemplate;
import rs.ey.taxserbia.activities.app.pages.types.GridSubHeaderBtnTemplate;
import rs.ey.taxserbia.activities.app.pages.types.IndustryOverviewGridTemplate;
import rs.ey.taxserbia.activities.app.pages.types.IndustryOverviewTemplate;
import rs.ey.taxserbia.activities.app.pages.types.NewsListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.OnlyImagesTemplate;
import rs.ey.taxserbia.activities.app.pages.types.PollsListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.SwipedListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.VideoListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorEarningsTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorServiceContractTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorYearTaxTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplateSecond;
import rs.ey.taxserbia.entities.pages.types.templates.GridSubHeaderPageTemplate;

import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_EARNINGS;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_SERVICE_CONTRACT;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_YEAR_TAX;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALENDAR_TEMPLATE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EVENT_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EXPANDABLE_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.GRID_BG_CONTACT_TEMPLATE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.GRID_ICON;
import static rs.ey.taxserbia.entities.pages.types.PagesType.INDUSTRY_OVERVIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.INDUSTRY_OVERVIEW_GRID;
import static rs.ey.taxserbia.entities.pages.types.PagesType.NEWS_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.PAGE_VIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.POLL_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.SWIPED_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.VIDEO_LIST;

/**
 * Created by darko on 28.6.18..
 */

public class GridSubHeaderBtnListTemplateAdapter extends RecyclerView.Adapter<GridSubHeaderBtnListTemplateAdapter.GridSubHeaderBtnListTemplateViewHolder> {

    private ArrayList<GridSubHeaderPageTemplate> data;
    private LayoutInflater inflater;
    private FragmentActivity activity;

    public GridSubHeaderBtnListTemplateAdapter(Context context, FragmentActivity activity, ArrayList<GridSubHeaderPageTemplate> data) {
        this.data = data;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public GridSubHeaderBtnListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_grid_sub_header_template, parent, false);
        return new GridSubHeaderBtnListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridSubHeaderBtnListTemplateViewHolder holder, final int position) {
        final GridSubHeaderPageTemplate gridSubHeaderPage = data.get(position);
        holder.txtGridSubHeaderPageTitle.setText(gridSubHeaderPage.getTitle());

        Picasso.with(activity)
                .load(gridSubHeaderPage.getImgUrl())
                .error(activity.getResources().getDrawable(R.drawable.aktuelnosti_cover))
                .into(holder.imgCover);

        holder.gridSubHeaderPageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (gridSubHeaderPage.getType()) {
                    case GRID_ICON:
                        intent = new Intent(activity, GridIconTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getId());
                        activity.startActivity(intent);
                        break;
                    case NEWS_LIST:
                        intent = new Intent(activity, NewsListTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getObjectId());
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case EXPANDABLE_LIST:
                        intent = new Intent(activity, ExpandableListTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getObjectId());
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case VIDEO_LIST:
                        intent = new Intent(activity, VideoListTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getObjectId());
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case POLL_LIST:
                        intent = new Intent(activity, PollsListTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case EVENT_LIST:
                        intent = new Intent(activity, EYEventsTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case SWIPED_LIST:
                        intent = new Intent(activity, SwipedListTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case CALCULATOR_EARNINGS:
                        intent = new Intent(activity, CalculatorEarningsTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case CALCULATOR_SERVICE_CONTRACT:
                        intent = new Intent(activity, CalculatorServiceContractTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case CALCULATOR_YEAR_TAX:
                        intent = new Intent(activity, CalculatorYearTaxTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case PAGE_VIEW:
                        intent = new Intent(activity, OnlyImagesTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getObjectId());
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case CALENDAR_TEMPLATE:
                        intent = new Intent(activity, CalendarTemplateSecond.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case GRID_BG_CONTACT_TEMPLATE:
                        intent = new Intent(activity, GridSubHeaderBtnTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getId());
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case INDUSTRY_OVERVIEW_GRID:
                        intent = new Intent(activity, IndustryOverviewGridTemplate.class);
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                    case INDUSTRY_OVERVIEW:
                        intent = new Intent(activity, IndustryOverviewTemplate.class);
                        intent.putExtra("id", gridSubHeaderPage.getId());
                        intent.putExtra("title", gridSubHeaderPage.getTitle());
                        activity.startActivity(intent);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class GridSubHeaderBtnListTemplateViewHolder extends RecyclerView.ViewHolder {
        TextView txtGridSubHeaderPageTitle;
        FrameLayout gridSubHeaderPageLayout;
        ImageView imgCover;

        public GridSubHeaderBtnListTemplateViewHolder(View itemView) {
            super(itemView);
            txtGridSubHeaderPageTitle = itemView.findViewById(R.id.txt_grid_sub_header_item_title);
            gridSubHeaderPageLayout = itemView.findViewById(R.id.grid_sub_header_item_layout);
            imgCover = itemView.findViewById(R.id.img_cover);
        }
    }
}
