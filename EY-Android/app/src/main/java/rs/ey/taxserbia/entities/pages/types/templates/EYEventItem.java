package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 24.9.18..
 */

public class EYEventItem {

    private String id;
    private String thumbnailUrl;
    private String publishDate;
    private String startDate;
    private String endDate;
    private String title;

    public EYEventItem(String id, String thumbnailUrl, String publishDate, String startDate, String endDate, String title) {
        this.id = id;
        this.thumbnailUrl = thumbnailUrl;
        this.publishDate = publishDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
