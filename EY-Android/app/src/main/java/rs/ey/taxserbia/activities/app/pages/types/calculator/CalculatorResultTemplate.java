package rs.ey.taxserbia.activities.app.pages.types.calculator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.CalculationResultsAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.CalculationResultItem;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.helpers.ShareContent;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B1_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B2_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B1_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B2_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B_VALUE;

public class CalculatorResultTemplate extends AppCompatActivity implements Responsable {

    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private VolleyService httpService;

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private String title;
    private String results;
    private String calculation;
    private String calculationValute;
    private String option3;
    private String activityName;
    private double inputValue;

    private TextView txtCalculationValute;
    private TextView txtCalculation;
    private TextView txtInputValue;
    private TextView txtOption1Label;
    private TextView txtOption3Label;
    private TextView txtOption3;
    private LinearLayout layoutOption3;

    private RecyclerView resultsRecyclerView;
    private ImageView btnShare;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_calculator_result);

        title = getIntent().getExtras().getString("title");
        results = getIntent().getExtras().getString("results");
        option3 = getIntent().getExtras().getString("option3");
        activityName = getIntent().getExtras().getString("activityName");
        calculation = getIntent().getExtras().getString("calculation");
        calculationValute = getIntent().getExtras().getString("calculationValute");
        inputValue = getIntent().getExtras().getDouble("inputValue");

        StatusBarUtil.setColor(CalculatorResultTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();
        bindResults();

        if (activityName.equals(CalculatorHelper.ACTIVITY_CONTRACT)) {
            layoutOption3.setVisibility(View.VISIBLE);
            txtOption3.setText(option3);
        } else {
            layoutOption3.setVisibility(View.GONE);
        }

        txtCalculationValute.setText(calculationValute);

        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);
        txtInputValue.setText(decimalFormat.format(inputValue) + "");

        switch (calculation) {
            case B1_N_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_value_1));
                break;
            case B2_N_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_value_2));
                break;
            case N_B1_VALUE:
            case N_B2_VALUE:
            case N_B_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_value_3));
                break;
            case B_N_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_value_4));
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        txtCalculationValute = findViewById(R.id.txt_calculation_valute);
        txtCalculation = findViewById(R.id.txt_calculation);
        txtInputValue = findViewById(R.id.txt_input_value);
        txtOption1Label = findViewById(R.id.txt_option_1_label);
        txtOption3Label = findViewById(R.id.txt_option_3_label);
        txtOption3 = findViewById(R.id.txt_option_3);
        layoutOption3 = findViewById(R.id.layout_option_3);

        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        resultsRecyclerView = findViewById(R.id.results_list_recycler_view);

        httpService = new VolleyService(this, this);

        btnShare = findViewById(R.id.btn_share);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(CalculatorResultTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray resultsArray = new JSONArray(results);
                    StringBuilder content = new StringBuilder();
                    content.append(title);
                    content.append("\n");
                    content.append("\n");
                    content.append(txtOption1Label.getText().toString());
                    content.append("\n");
                    content.append("\n");
                    content.append(txtCalculationValute.getText().toString());
                    content.append("\n");
                    content.append("\n");
                    content.append(txtCalculation.getText().toString());
                    content.append("\n");
                    content.append(txtInputValue.getText().toString());
                    content.append("\n");
                    content.append("\n");

                    if (activityName.equals(CalculatorHelper.ACTIVITY_CONTRACT)) {
                        content.append(txtOption3Label.getText().toString());
                        content.append("\n");
                        content.append(txtOption3.getText().toString());
                        content.append("\n");
                        content.append("\n");
                    }

                    for (int i = 0; i < resultsArray.length(); i++) {
                        JSONObject resultObject = resultsArray.getJSONObject(i);
                        String resultValue = resultObject.getString("key") + ": " + resultObject.getString("value");
                        content.append(resultValue);
                        content.append("\n");
                    }
                    ShareContent.share(CalculatorResultTemplate.this, content);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindResults() {
        try {
            JSONArray resultsArray = new JSONArray(results);
            ArrayList<CalculationResultItem> resultItems = new ArrayList<>();

            for (int i = 0; i < resultsArray.length(); i++) {
                JSONObject resultObject = resultsArray.getJSONObject(i);
                CalculationResultItem resultItem = new CalculationResultItem(resultObject.getString("key"), resultObject.getString("value"));
                resultItems.add(resultItem);
            }

            resultsRecyclerView.setHasFixedSize(true);
            final CalculationResultsAdapter calculationResultsAdapter = new CalculationResultsAdapter(getApplicationContext(), resultItems);
            resultsRecyclerView.setAdapter(calculationResultsAdapter);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            resultsRecyclerView.setLayoutManager(layoutManager);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(CalculatorResultTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            JSONArray entity = res.getJSONArray("entity");

            for (int i = 0; i < entity.length(); i++) {
                JSONObject searchItem = entity.getJSONObject(i);
                createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
            }

            updateSearchListResultRecyclerView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        try {
            Toast.makeText(this, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
