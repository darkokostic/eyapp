package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactSingleViewTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.ContactItem;

/**
 * Created by darko on 6.8.18..
 */

public class ContactListTemplateAdapter extends RecyclerView.Adapter<ContactListTemplateAdapter.ContactListTemplateViewHolder> {

    private Context context;
    private ArrayList<ContactItem> data;
    private LayoutInflater inflater;

    public ContactListTemplateAdapter(Context context, ArrayList<ContactItem> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ContactListTemplateAdapter.ContactListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_contacts_item_template, parent, false);
        return new ContactListTemplateAdapter.ContactListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactListTemplateAdapter.ContactListTemplateViewHolder holder, final int position) {
        final ContactItem contactItem = data.get(position);

        Drawable defaultIcon = context.getResources().getDrawable(R.drawable.contact_thumbnail);
        String thumbnailUrl = contactItem.getThumbnailUrl();

        holder.txtName.setText(contactItem.getName());
        holder.txtPosition.setText(contactItem.getPosition());

        if (thumbnailUrl == null) {
            holder.imgThumbnail.setImageDrawable(defaultIcon);
        } else {
            Picasso.with(context)
                    .load(thumbnailUrl)
                    .error(defaultIcon)
                    .into(holder.imgThumbnail);
        }

        holder.contactItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContactSingleViewTemplate.class);
                intent.putExtra("id", contactItem.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ContactListTemplateViewHolder extends RecyclerView.ViewHolder {
        LinearLayout contactItemLayout;
        ImageView imgThumbnail;
        TextView txtName;
        TextView txtPosition;

        public ContactListTemplateViewHolder(View itemView) {
            super(itemView);
            contactItemLayout = itemView.findViewById(R.id.contact_item_layout);
            imgThumbnail = itemView.findViewById(R.id.img_contacts_thumbnail);
            txtName = itemView.findViewById(R.id.txt_name);
            txtPosition = itemView.findViewById(R.id.txt_position);
        }
    }
}
