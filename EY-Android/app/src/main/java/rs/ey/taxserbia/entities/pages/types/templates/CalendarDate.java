package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 17.11.18..
 */

public class CalendarDate {

    private int dateNumber;

    private boolean isSelected;

    public CalendarDate(int dateNumber, boolean isSelected) {
        this.dateNumber = dateNumber;
        this.isSelected = isSelected;
    }

    public int getDateNumber() {
        return dateNumber;
    }

    public void setDateNumber(int dateNumber) {
        this.dateNumber = dateNumber;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
