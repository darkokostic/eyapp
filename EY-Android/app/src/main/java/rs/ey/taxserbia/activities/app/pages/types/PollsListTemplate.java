package rs.ey.taxserbia.activities.app.pages.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.PollAnswerItemClickHandler;
import rs.ey.taxserbia.adapters.page.types.PollAnswersAdapter;
import rs.ey.taxserbia.adapters.page.types.PollsArchiveListTemplateAdapter;
import rs.ey.taxserbia.adapters.page.types.PollsListItemClickHandler;
import rs.ey.taxserbia.adapters.page.types.PollsListTemplateAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.PollAnswerItem;
import rs.ey.taxserbia.entities.pages.types.templates.PollItem;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

public class PollsListTemplate extends AppCompatActivity implements Responsable,
        PollsListItemClickHandler, PollAnswerItemClickHandler {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;
    private Responsable responsable;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/polls";
    private final static String HTTP_URL_POLL_SINGLE_VIEW = "https://ey.nbgcreator.com/api/polls/single";
    private final static String HTTP_URL_POLL_VOTING = "https://ey.nbgcreator.com/api/polls/vote";
    private final static String HTTP_URL_ARCHIVE_POLLS = "https://ey.nbgcreator.com/api/polls/archive";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";

    private final static int REQUEST_TYPE_SEARCH = 4;
    private final static int REQUEST_TYPE_POLL_LIST = 0;
    private final static int REQUEST_TYPE_POLL_SINGLE_VIEW = 1;
    private final static int REQUEST_TYPE_POLL_VOTING = 2;
    private final static int REQUEST_TYPE_ARCHIVE_POST_LIST = 3;

    private LinearLayout contentView;
    private LinearLayout layoutArchivePolls;
    private ImageView btnArchive;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    private RecyclerView pollsListRecyclerView;
    private ArrayList<PollItem> polls;

    private ArrayList<PollItem> archivePolls;
    private RecyclerView archivePollsListRecyclerView;
    private ImageView imgArrowArchive;

    private LinearLayout pollSingleViewLayout;
    private LinearLayout pollSingleViewItemLayout;
    private TextView txtPollDate;
    private TextView txtPollQuestion;

    private RecyclerView pollAnswersRecyclerView;
    private ArrayList<PollAnswerItem> pollAnswers;

    private ImageView btnInfo;
    private LinearLayout layoutInfo;
    private LinearLayout btnCloseInfo;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_polls_list);

        StatusBarUtil.setColor(PollsListTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (CheckInternetConnection.isOnline(this)) {
            requestPollsList();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);
        txtPollDate = findViewById(R.id.txt_poll_date);
        txtPollQuestion = findViewById(R.id.txt_poll_question);
        pollsListRecyclerView = findViewById(R.id.polls_list_recycler_view);
        archivePollsListRecyclerView = findViewById(R.id.archive_polls_list_recycler_view);
        pollAnswersRecyclerView = findViewById(R.id.poll_answers_recycler_view);
        imgArrowArchive = findViewById(R.id.arrow_archive);

        TextView txtTitle = findViewById(R.id.txt_title);
        String title = getIntent().getExtras().getString("title");
        txtTitle.setText(title);

        responsable = this;

        contentView = findViewById(R.id.content_view);
        layoutArchivePolls = findViewById(R.id.layout_archive_polls);
        btnArchive = findViewById(R.id.btn_archive);
        pollSingleViewLayout = findViewById(R.id.poll_single_view_layout);
        pollSingleViewItemLayout = findViewById(R.id.layout_poll_item);
        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        contentView.setVisibility(View.GONE);
        pollSingleViewLayout.setVisibility(View.GONE);
        layoutArchivePolls.setVisibility(View.GONE);
        imgArrowArchive.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_POLL_LIST);

        btnInfo = findViewById(R.id.btn_info);
        layoutInfo = findViewById(R.id.layout_info);
        btnCloseInfo = findViewById(R.id.btn_close_info);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PollsListTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(PollsListTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PollsListTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PollsListTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PollsListTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        pollSingleViewItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                httpService = new VolleyService(responsable, getApplicationContext(), REQUEST_TYPE_POLL_LIST);
                pollSingleViewLayout.setVisibility(View.GONE);
            }
        });

        btnArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutArchivePolls.getVisibility() == View.GONE) {
                    layoutArchivePolls.setVisibility(View.VISIBLE);
                    imgArrowArchive.setVisibility(View.VISIBLE);
                    loadingLayout.setVisibility(View.VISIBLE);
                    requestArchivePolls();
                } else {
                    layoutArchivePolls.setVisibility(View.GONE);
                    imgArrowArchive.setVisibility(View.GONE);
                }
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutInfo.setVisibility(View.VISIBLE);
            }
        });

        btnCloseInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutInfo.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(PollsListTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void requestPollsList() {
        httpService = new VolleyService(this, this, REQUEST_TYPE_POLL_LIST);
        polls = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort;
        httpService.get(HTTP_URL + params);
    }

    private void createPollItem(ArrayList<PollItem> pollItems, String id, String date, String name) {
        PollItem pollItem = new PollItem(id, date, name);
        pollItems.add(pollItem);
    }

    private void createPollAnswerItem(ArrayList<PollAnswerItem> pollAnswers, String id, String votes, String option, String answerCountChar) {
        PollAnswerItem pollAnswerItem = new PollAnswerItem(id, votes, option, answerCountChar);
        pollAnswers.add(pollAnswerItem);
    }

    private void createListLayout() {
        pollsListRecyclerView.setHasFixedSize(true);
        final PollsListTemplateAdapter pollsListTemplateAdapter = new PollsListTemplateAdapter(this, polls, this);
        pollsListRecyclerView.setAdapter(pollsListTemplateAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        pollsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void createArchiveListLayout() {
        archivePollsListRecyclerView.setHasFixedSize(true);
        final PollsArchiveListTemplateAdapter pollsListTemplateAdapter = new PollsArchiveListTemplateAdapter(this, archivePolls, this);
        archivePollsListRecyclerView.setAdapter(pollsListTemplateAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        archivePollsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void createAnswersListLayout() {
        pollAnswersRecyclerView.setHasFixedSize(true);
        final PollAnswersAdapter pollAnswersAdapter = new PollAnswersAdapter(this, pollAnswers, this);
        pollAnswersRecyclerView.setAdapter(pollAnswersAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        pollAnswersRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestPollSingleView(String id) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_POLL_SINGLE_VIEW);
        pollAnswers = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort
                + "&id=" + id;
        httpService.get(HTTP_URL_POLL_SINGLE_VIEW + params);
    }

    private void requestPollVoting(String id) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_POLL_VOTING);
        JSONObject params = new JSONObject();
        try {
            params.put("id", id);
            params.put("lang", EyApplication.langShort);
            httpService.post(HTTP_URL_POLL_VOTING, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void requestArchivePolls() {
        httpService = new VolleyService(this, this, REQUEST_TYPE_ARCHIVE_POST_LIST);
        archivePolls = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort;
        httpService.get(HTTP_URL_ARCHIVE_POLLS + params);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_TYPE_POLL_LIST) {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject poll = entity.getJSONObject(i);
                    createPollItem(polls, poll.getString("id"), poll.getString("date"),
                            poll.getString("name"));
                }

                createListLayout();

                contentView.setVisibility(View.VISIBLE);
                loadingLayout.setVisibility(View.GONE);
            } else if (requestType == REQUEST_TYPE_POLL_SINGLE_VIEW) {
                JSONObject entity = res.getJSONObject("entity");
                JSONArray answers = entity.getJSONArray("options");

                for (int i = 0; i < answers.length(); i++) {
                    JSONObject answer = answers.getJSONObject(i);
                    String indicator = EyApplication.POLL_ANSWERS_INDICATORS_EN[i];
                    if (EyApplication.langShort.equals("sr")) {
                        indicator = EyApplication.POLL_ANSWERS_INDICATORS_SR[i];
                    }
                    createPollAnswerItem(pollAnswers, answer.getString("id"), answer.getString("votes"),
                            answer.getString("option"), indicator);
                }

                createAnswersListLayout();
            } else if (requestType == REQUEST_TYPE_POLL_VOTING) {
                pollSingleViewLayout.setVisibility(View.GONE);
                Toast.makeText(this, getResources().getString(R.string.polls_thank_you_for_voting_msg), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(PollsListTemplate.this, PollVoteThankYouTemplate.class);
                intent.putExtra("date", date);
                startActivity(intent);
            } else if (requestType == REQUEST_TYPE_ARCHIVE_POST_LIST) {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject poll = entity.getJSONObject(i);
                    createPollItem(archivePolls, poll.getString("id"), poll.getString("date"),
                            poll.getString("name"));
                }

                createArchiveListLayout();

                loadingLayout.setVisibility(View.GONE);
            } else {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
            }
        } catch (JSONException e) {
            loadingLayout.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void openPollSingleView(PollItem item) {
        txtPollQuestion.setText(item.getName());
        txtPollDate.setText(item.getDate());

        date = item.getDate();

        pollSingleViewLayout.setVisibility(View.VISIBLE);
        requestPollSingleView(item.getId());
    }

    @Override
    public void openPollAnswerClick(PollAnswerItem item) {
        requestPollVoting(item.getId());
    }
}
