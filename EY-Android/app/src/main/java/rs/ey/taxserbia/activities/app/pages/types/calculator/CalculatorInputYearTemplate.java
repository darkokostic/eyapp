package rs.ey.taxserbia.activities.app.pages.types.calculator;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.helpers.NumberTextWatcherForThousand;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B1_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B2_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_VALUE;

public class CalculatorInputYearTemplate extends AppCompatActivity implements Responsable {

    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private final static String HTTP_CONFIG_URL = "https://ey.nbgcreator.com/api/config";

    private final static int REQUEST_TYPE_SEARCH = 0;
    private final static int REQUEST_TYPE_1 = 1;
    private final static int REQUEST_TYPE_2 = 2;
    private final static int REQUEST_TYPE_3 = 3;

    private VolleyService httpService;

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private TextView txtOption1;
    private TextView txtOption2;

    private String title;
    private String option1;
    private String option2;

    private LinearLayout loadingLayout;
    private ProgressBar progressBar;
    private EditText etInput1;
    private EditText etInput2;
    private EditText etExchangeRate;
    private LinearLayout btnCalculate;
    private String calculation;
    private String calculationValute;

    private double inputValue1;
    private int inputValue2;

    int counter = 0;

    double e12;
    double e14;
    double d16;
    double d18;
    double d20;
    double d17;
    double d19;
    double d21;
    double l9;
    double l18;
    double l19;

    double e10;
    double j14;
    double e21;
    double e11;
    double e13;
    double e16;
    double e18;
    double e20;
    double f10;
    double f11;
    double f14;
    double f16;
    double f18;
    double f20;
    double f12;
    double f13;
    double f15;
    double f17;
    double f19;
    double f21;
    double j10;
    double l12;
    double j11;
    double j12;
    double j16;
    double j17;
    double j13;
    double j15;
    double j18;
    double l15;
    double j19;
    double j20;

    double e15;
    double e17;
    double e19;

    private ScrollView scrollView;
    private LinearLayout layoutInfo;
    private ImageView btnInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_calculator_input_year);

        title = getIntent().getExtras().getString("title");
        option1 = getIntent().getExtras().getString("option1");
        option2 = getIntent().getExtras().getString("option2");
        calculation = getIntent().getExtras().getString("calculation");
        calculationValute = getIntent().getExtras().getString("calculationValute");

        StatusBarUtil.setColor(CalculatorInputYearTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (calculationValute.equals(CalculatorHelper.RSD_VALUE)) {
            etExchangeRate.setVisibility(View.GONE);
        }

        switch (calculation) {
            case N_VALUE:
                etInput1.setHint(getResources().getString(R.string.calc_year_input_1_hint1));
                break;
            case B1_VALUE:
                etInput1.setHint(getResources().getString(R.string.calc_year_input_1_hint2));
                break;
            case B2_VALUE:
                etInput1.setHint(getResources().getString(R.string.calc_year_input_1_hint3));
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);
        loadingLayout.setVisibility(View.GONE);

        txtOption1 = findViewById(R.id.txt_option_1);
        txtOption2 = findViewById(R.id.txt_option_2);

        txtOption1.setText(option1);
        String billingTypeText = getResources().getText(R.string.calc_billing_type_label) + " " + option2;
        txtOption2.setText(billingTypeText);
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        etInput1 = findViewById(R.id.et_input1);
        etInput2 = findViewById(R.id.et_input2);
        etExchangeRate = findViewById(R.id.et_input3);
        btnCalculate = findViewById(R.id.btn_calculate);

        scrollView = findViewById(R.id.scroll_view);
        layoutInfo = findViewById(R.id.layout_info);
        btnInfo = findViewById(R.id.btn_info);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputYearTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(CalculatorInputYearTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        if (CheckInternetConnection.isOnline(getApplicationContext())) {
                            requestSearch(query);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputYearTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputYearTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputYearTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputValue1 = Float.parseFloat(NumberTextWatcherForThousand.trimCommaOfString(etInput1.getText().toString()));
                inputValue2 = Integer.parseInt(etInput2.getText().toString());

                if (CheckInternetConnection.isOnline(getApplicationContext())) {
                    initValues();
                    switch (calculation) {
                        case N_VALUE:
                            countN();
                            break;
                        case B1_VALUE:
                            countB1();
                            break;
                        case B2_VALUE:
                            countB2();
                            break;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0, layoutInfo.getTop());
            }
        });

        etInput1.addTextChangedListener(new NumberTextWatcherForThousand(etInput1));
        etInput2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if(s.length() == 0) {
                    etInput2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                } else {
                    etInput2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    private void initValues() {
        counter = 0;

        e12 = 0;
        e14 = 0;
        d16 = 0;
        d18 = 0;
        d20 = 0;
        d17 = 0;
        d19 = 0;
        d21 = 0;
        l9 = 0;
        l18 = 0;
        l19 = 0;

        e10 = 0;
        j14 = 0;
        e21 = 0;
        e11 = 0;
        e13 = 0;
        f10 = 0;
        f11 = 0;
        f14 = 0;
        f12 = 0;
        f13 = 0;
        f15 = 0;
        f17 = 0;
        f19 = 0;
        f21 = 0;
        j10 = 0;
        l12 = 0;
        j11 = 0;
        j12 = 0;
        j16 = 0;
        j17 = 0;
        j13 = 0;
        j15 = 0;
        j18 = 0;
        l15 = 0;
        j19 = 0;
        j20 = 0;

        e15 = 0;
        e17 = 0;
        e19 = 0;
        e16 = 0;
        e18 = 0;
        e20 = 0;
        f16 = 0;
        f18 = 0;
        f20 = 0;

        if (calculationValute.equals(CalculatorHelper.RSD_VALUE)) {
            e10 = inputValue1;
        } else {
            float exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
            e10 = inputValue1 * exchangeRate;
        }
        j14 = inputValue2;
    }

    private void countN() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_1);
        String params = "?keys[]=mob_calculator_max_base_social_contributions&keys[]=mob_calculator_non_taxable_amount&keys[]=mob_calculator_salary_tax&keys[]=mob_calculator_social_contributions_employee&keys[]=mob_calculator_social_contributions_employer&keys[]=mob_calculator_average_annual_income&keys[]=mob_calculator_tax_rates_anual_1&keys[]=mob_calculator_tax_rates_anual_2";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    private void countB1() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_2);
        String params = "?keys[]=mob_calculator_max_base_social_contributions&keys[]=mob_calculator_non_taxable_amount&keys[]=mob_calculator_salary_tax&keys[]=mob_calculator_social_contributions_employee&keys[]=mob_calculator_social_contributions_employer&keys[]=mob_calculator_average_annual_income&keys[]=mob_calculator_tax_rates_anual_1&keys[]=mob_calculator_tax_rates_anual_2";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    private void countB2() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_3);
        String params = "?keys[]=mob_calculator_max_base_social_contributions&keys[]=mob_calculator_non_taxable_amount&keys[]=mob_calculator_salary_tax&keys[]=mob_calculator_social_contributions_employee&keys[]=mob_calculator_social_contributions_employer&keys[]=mob_calculator_average_annual_income&keys[]=mob_calculator_tax_rates_anual_1&keys[]=mob_calculator_tax_rates_anual_2";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(CalculatorInputYearTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        loadingLayout.setVisibility(View.GONE);
        switch (requestType) {
            case REQUEST_TYPE_SEARCH:
                responseSearch(res);
                break;
            case REQUEST_TYPE_1:
                responseN(res);
                break;
            case REQUEST_TYPE_2:
                responseB1(res);
                break;
            case REQUEST_TYPE_3:
                responseB2(res);
                break;
        }
    }

    private void responseN(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            e11 = Double.parseDouble(entity.getString("mob_calculator_max_base_social_contributions"));
            e13 = Double.parseDouble(entity.getString("mob_calculator_non_taxable_amount"));
            d17 = Double.parseDouble(entity.getString("mob_calculator_salary_tax"));
            d19 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employee"));
            d21 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employer"));
            l9 = Double.parseDouble(entity.getString("mob_calculator_average_annual_income"));
            l18 = Double.parseDouble(entity.getString("mob_calculator_tax_rates_anual_1"));
            l19 = Double.parseDouble(entity.getString("mob_calculator_tax_rates_anual_2"));

            counter = 0;
            calculateNB1B2(counter);

            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            if (exchangeRate != 0) {
                j20 = j20 / exchangeRate;
            }
            Intent intent = new Intent(CalculatorInputYearTemplate.this, CalculatorResultYearTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue1", inputValue1);
            intent.putExtra("inputValue2", inputValue2);
            intent.putExtra("result", j20);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calculateNB1B2(int counter) {
        double d17 = this.d17 / 100;
        double d19 = this.d19 / 100;
        double d21 = this.d21 / 100;
        double l18 = this.l18 / 100;
        double l19 = this.l19 / 100;

        if (e14 > e11) {
            e12 = e11;
        } else {
            e12 = e14;
        }

        if (e14 > e11) {
            e14 = (e10 - (e13 * d17) + (e11 * d19)) / (1 - d17);
        } else {
            e14 = (e10 - e13 * d17) / (1 - d19 - d17);
        }

        e15 = e14 - e17 - e19;

        e17 = (e14 - e13) * d17;

        if (e14 > e12) {
            e19 = e12 * d19;
        } else {
            e19 = e14 * d19;
        }

        if (e14 > e19) {
            e21 = e14 * d21;
        } else {
            e21 = e12 * d21;
        }

        f10 = e10 * 12;

        f11 = e11 * 12;

        if (f14 > f11) {
            f12 = f11;
        } else {
            f12 = f14;
        }

        f13 = e13 * 12;

        if (f14 > f11) {
            f14 = (f10 - (f13 * d17) + (f11 * d19)) / (1 - d17);
        } else {
            f14 = (f10 - f13 * d17) / (1 - d19 - d17);
        }

        f15 = f14 - f17 - f19;

        f17 = (f14 - f13) * d17;

        if (f14 > f12) {
            f19 = f12 * d19;
        } else {
            f19 = f14 * d19;
        }

        if (f14 > f12) {
            f21 = f12 * d21;
        } else {
            f21 = f14 * d21;
        }

        l12 = l9 * 3;

        l15 = l9 * 6;

        j10 = f15 - f13;

        j11 = l12;

        if (j10 < j11) {
            j12 = 0;
        } else {
            j12 = j10 - j11;
        }

        j13 = j12 * 0.5;

        if (j14 > 0) {
            j15 = j16 + j17;
        } else {
            j15 = j16;
        }

        j16 = l9 * 0.4;

        j17 = l9 * 0.15 * j14;

        if (j13 < j15) {
            j18 = j13;
        } else {
            j18 = j15;
        }

        j19 = j12 - j18;

        if (j10 < j11) {
            j20 = 0;
        } else {
            if (j19 <= l15) {
                j20 = j19 * l18;
            } else {
                j20 = l15 * l18 + (j19 - l15) * l19;
            }
        }

        counter++;
        if (counter < 100) {
            calculateNB1B2(counter);
        }
    }

    private void responseB1(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            e11 = Double.parseDouble(entity.getString("mob_calculator_max_base_social_contributions"));
            e13 = Double.parseDouble(entity.getString("mob_calculator_non_taxable_amount"));
            d16 = Double.parseDouble(entity.getString("mob_calculator_salary_tax"));
            d18 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employee"));
            d20 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employer"));
            l9 = Double.parseDouble(entity.getString("mob_calculator_average_annual_income"));
            l18 = Double.parseDouble(entity.getString("mob_calculator_tax_rates_anual_1"));
            l19 = Double.parseDouble(entity.getString("mob_calculator_tax_rates_anual_2"));

            counter = 0;
            calculateB1(counter);

            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            if (exchangeRate != 0) {
                j20 = j20 / exchangeRate;
            }
            Intent intent = new Intent(CalculatorInputYearTemplate.this, CalculatorResultYearTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue1", inputValue1);
            intent.putExtra("inputValue2", inputValue2);
            intent.putExtra("result", j20);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calculateB1(int counter) {
        double l18 = this.l18 / 100;
        double l19 = this.l19 / 100;
        double d16 = this.d16 / 100;
        double d18 = this.d18 / 100;
        double d20 = this.d20 / 100;

        if (e10 > e11) {
            e12 = e11;
        } else {
            e12 = e10;
        }

        e14 = e10 - (e10 - e13) * d16 - e12 * d18;

        e16 = (e10 - e13) * d16;

        e18 = e10 * d18;
        if (e10 > e12) {
            e18 = e12 * d18;
        }

        e20 = e10 * d20;
        if (e10 > e12) {
            e20 = e12 * d20;
        }

        f10 = e10 * 12;
        f11 = e11 * 12;

        if (f10 > f11) {
            f12 = f11;
        } else {
            f12 = f10;
        }

        f13 = e13 * 12;
        f14 = f10 - (f10 - f13) * d16 - (f12 * d18);
        f16 = (f10 - f13) * d16;

        if (f10 > f12) {
            f18 = f12 * d18;
        } else {
            f18 = f10 * d18;
        }

        if (f10 > f12) {
            f20 = f12 * d20;
        } else {
            f20 = f10 * d20;
        }

        l12 = l9 * 3;
        l15 = l9 * 6;

        j10 = f14 - f13;
        j11 = l12;

        if (j10 < j11) {
            j12 = 0;
        } else {
            j12 = j10 - j11;
        }

        j13 = j12 * 0.5;

        if (j14 > 0) {
            j15 = j16 + j17;
        } else {
            j15 = j16;
        }

        j16 = l9 * 0.4;

        j17 = l9 * 0.15 * j14;

        if (j13 < j15) {
            j18 = j13;
        } else {
            j18 = j15;
        }

        j19 = j12 - j18;

        if (j10 < j11) {
            j20 = 0;
        } else {
            if (j19 <= l15) {
                j20 = j19 * l18;
            } else {
                j20 = l15 * l18 + (j19 - l15) * l19;
            }
        }

        counter++;
        if (counter < 100) {
            calculateB1(counter);
        }
    }

    private void responseB2(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            e12 = Double.parseDouble(entity.getString("mob_calculator_max_base_social_contributions"));
            e14 = Double.parseDouble(entity.getString("mob_calculator_non_taxable_amount"));
            d17 = Double.parseDouble(entity.getString("mob_calculator_salary_tax"));
            d19 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employee"));
            d21 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employer"));
            l9 = Double.parseDouble(entity.getString("mob_calculator_average_annual_income"));
            l18 = Double.parseDouble(entity.getString("mob_calculator_tax_rates_anual_1"));
            l19 = Double.parseDouble(entity.getString("mob_calculator_tax_rates_anual_2"));

            counter = 0;
            calculateB2(counter);

            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            if (exchangeRate != 0) {
                j20 = j20 / exchangeRate;
            }
            Intent intent = new Intent(CalculatorInputYearTemplate.this, CalculatorResultYearTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue1", inputValue1);
            intent.putExtra("inputValue2", inputValue2);
            intent.putExtra("result", j20);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calculateB2(int counter) {
        double d21 = this.d21 / 100;
        double d19 = this.d19 / 100;
        double d17 = this.d17 / 100;
        double l18 = this.l18 / 100;
        double l19 = this.l19 / 100;

        e11 = e10 - e21;

        if (e11 > e12) {
            e13 = e12;
        } else {
            e13 = e11;
        }

        e15 = e11 - (e11 - e14) * d17 - e13 * d19;
        e17 = (e11 - e14) * d17;

        if (e11 > e13) {
            e19 = e13 * d19;
        } else {
            e19 = e11 * d19;
        }

        if (e11 > e13) {
            e21 = e13 * d21;
        } else {
            e21 = e11 * d21;
        }

        f10 = e10 * 12;
        f11 = f10 - f21;
        f12 = e12 * 12;

        if (f11 > f12) {
            f13 = f12;
        } else {
            f13 = f11;
        }

        f14 = e14 * 12;
        f15 = f11 - ((f11 - f14) * d17) - (f13 * d19);

        f17 = (f11 - f14) * d17;

        if (f11 > f13) {
            f19 = f13 * d19;
        } else {
            f19 = f11 * d19;
        }

        if (f11 > f13) {
            f21 = f13 * d21;
        } else {
            f21 = f11 * d21;
        }

        l12 = l9 * 3;
        l15 = l9 * 6;

        j10 = f15 - f14;
        j11 = l12;

        j12 = j10 - j11;
        if (j10 < j11) {
            j12 = 0;
        }

        j13 = j12 * 0.5;

        j15 = j16;
        if (j14 > 0) {
            j15 = j16 + j17;
        }

        j16 = l9 * 0.4;
        j17 = l9 * 0.15 * j14;

        j18 = j15;
        if (j13 < j15) {
            j18 = j13;
        }

        j19 = j12 - j18;

        if (j10 < j11) {
            j20 = 0;
        } else {
            if (j19 <= l15) {
                j20 = j19 * l18;
            } else {
                j20 = l15 * l18 + (j19 - l15) * l19;
            }
        }

        counter++;
        if (counter < 100) {
            calculateB2(counter);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        try {
            Toast.makeText(this, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void responseSearch(JSONObject res) {
        try {
            JSONArray entity = res.getJSONArray("entity");

            for (int i = 0; i < entity.length(); i++) {
                JSONObject searchItem = entity.getJSONObject(i);
                createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
            }

            updateSearchListResultRecyclerView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
