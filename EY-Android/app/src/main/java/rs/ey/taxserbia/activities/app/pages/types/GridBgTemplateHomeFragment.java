package rs.ey.taxserbia.activities.app.pages.types;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.adapters.page.types.GridBgListTemplateAdapter;
import rs.ey.taxserbia.entities.pages.types.templates.GridBgPageTemplate;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

public class GridBgTemplateHomeFragment extends Fragment implements Responsable {

    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/menu";
    private VolleyService httpService;
    private RecyclerView gridBgListRecyclerView;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;
    private ArrayList<GridBgPageTemplate> gridBgPages;
    private Context context;
    private ImageView bgLayout;

    public GridBgTemplateHomeFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.template_grid_bg_fragment, container, false);

        gridBgListRecyclerView = view.findViewById(R.id.grid_bg_recycler_view);
        loadingLayout = view.findViewById(R.id.loading_layout);
        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);
        loadingLayout.setVisibility(View.VISIBLE);
        gridBgListRecyclerView.setVisibility(View.INVISIBLE);
        httpService = new VolleyService(this, this.getContext());
        bgLayout = view.findViewById(R.id.bg_layout);
        bgLayout.setVisibility(View.GONE);

        context = getContext();

        if (CheckInternetConnection.isOnline(this.getContext())) {
            requestPagesList();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this.getContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void requestPagesList() {
        gridBgPages = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort;
        httpService.get(HTTP_URL + params);
    }

    private void createListLayout() {
        if (getActivity() != null) {
            gridBgListRecyclerView.setHasFixedSize(true);
            final GridBgListTemplateAdapter gridBgListAdapter = new GridBgListTemplateAdapter(getContext(), getActivity(), gridBgPages);
            gridBgListRecyclerView.setAdapter(gridBgListAdapter);

            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
            gridBgListRecyclerView.setLayoutManager(layoutManager);
        }
    }

    private void createSubPage(ArrayList<GridBgPageTemplate> subPagesList, String id, String objectId, String title, String imageUrl, String type) {
        GridBgPageTemplate subPage = new GridBgPageTemplate(id, objectId, title, imageUrl, type);
        subPagesList.add(subPage);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            JSONObject entity = res.getJSONObject("entity");

            JSONArray pagesList = entity.getJSONArray("list");
            for (int i = 0; i < pagesList.length(); i++) {
                JSONObject page = pagesList.getJSONObject(i);
                createSubPage(gridBgPages, page.getString("id"), page.getString("object_id"), page.getString("title"), page.getString("image"), page.getString("type"));
            }

            createListLayout();

            gridBgListRecyclerView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
            bgLayout.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(context, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
