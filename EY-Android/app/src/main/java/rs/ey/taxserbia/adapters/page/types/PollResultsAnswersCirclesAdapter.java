package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.entities.pages.types.templates.PollAnswerResultItem;

/**
 * Created by darko on 28.6.18..
 */

public class PollResultsAnswersCirclesAdapter extends RecyclerView.Adapter<PollResultsAnswersCirclesAdapter.PollResultsAnswersCirclesViewHolder> {

    private ArrayList<PollAnswerResultItem> data;
    private LayoutInflater inflater;
    private Context context;

    public PollResultsAnswersCirclesAdapter(Context context, ArrayList<PollAnswerResultItem> data) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public PollResultsAnswersCirclesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_answer_result_item_circle_graph_poll_list_template, parent, false);
        return new PollResultsAnswersCirclesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PollResultsAnswersCirclesViewHolder holder, final int position) {
        final PollAnswerResultItem pollAnswerResultItem = data.get(position);

        int percentage = (int) Float.parseFloat(pollAnswerResultItem.getPercentage());
        holder.progressBar.setDonut_progress(percentage + "");
        holder.progressBar.setPrefixText(pollAnswerResultItem.getAnswerCountChar() + "  ");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PollResultsAnswersCirclesViewHolder extends RecyclerView.ViewHolder {

        DonutProgress progressBar;

        public PollResultsAnswersCirclesViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }
}
