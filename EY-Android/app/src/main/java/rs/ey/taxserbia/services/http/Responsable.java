package rs.ey.taxserbia.services.http;

import org.json.JSONObject;

/**
 * Created by darko on 2.8.18..
 */

public interface Responsable {
    void onSuccessResponse(JSONObject res, int requestType);
    void onErrorResponse(JSONObject err);
}
