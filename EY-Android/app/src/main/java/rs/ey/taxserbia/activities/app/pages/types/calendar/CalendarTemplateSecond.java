package rs.ey.taxserbia.activities.app.pages.types.calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.CalendarDatesAdapter;
import rs.ey.taxserbia.adapters.page.types.CalendarObligationsAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.CalendarDate;
import rs.ey.taxserbia.entities.pages.types.templates.CalendarObligation;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.calendar.CalendarDateClickable;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

public class CalendarTemplateSecond extends AppCompatActivity implements Responsable, CalendarDateClickable {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;
    private String title;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/calendar/days";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private final static String HTTP_OBLIGATIONS_URL = "https://ey.nbgcreator.com/api/calendar/list";

    private final static int REQUEST_DATES_TYPE = 1;
    private final static int REQUEST_SEARCH_TYPE = 2;
    private final static int REQUEST_OBLIGATIONS_TYPE = 3;

    private LinearLayout contentView;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    private RecyclerView obligationsRecyclerView;
    private ArrayList<CalendarObligation> obligationsList;
    private RecyclerView datesRecyclerView;
    private ArrayList<CalendarDate> datesList;
    private CalendarDatesAdapter datesAdapter;
    private ImageView btnCalendar;
    private TextView txtYear;
    private TextView txtMonthName;
    private LinearLayout btnYearCalendar;
    private LinearLayout btnArrowRight;
    private LinearLayout btnArrowLeft;
    private ScrollView layoutCalendarMonths;
    private LinearLayout layoutJanuary;
    private LinearLayout layoutFebruary;
    private LinearLayout layoutMarch;
    private LinearLayout layoutApril;
    private LinearLayout layoutMay;
    private LinearLayout layoutJune;
    private LinearLayout layoutJuly;
    private LinearLayout layoutAugust;
    private LinearLayout layoutSeptember;
    private LinearLayout layoutOctober;
    private LinearLayout layoutNovember;
    private LinearLayout layoutDecember;
    private int currentYear;
    private int currentMonth;
    private int currentDate;
    private int chosenYear;
    private int chosenMonth;

    private int selectedDate;
    private int selectedMonth;
    private int selectedYear;
    private boolean isMonthChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_calendar_second);

        title = getIntent().getExtras().getString("title");

        currentDate = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        selectedDate = currentDate;

        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        chosenYear = currentYear;
        selectedYear = currentYear;

        currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        chosenMonth = currentMonth;
        selectedMonth = currentMonth;

        StatusBarUtil.setColor(CalendarTemplateSecond.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();
        setActiveCurrentMonth(false);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    @SuppressLint("SetTextI18n")
    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        contentView = findViewById(R.id.content_view);
        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        txtMonthName = findViewById(R.id.txt_month_name);
        txtYear = findViewById(R.id.txt_year);
        txtYear.setText(Integer.toString(currentYear));

        btnCalendar = findViewById(R.id.btn_calendar);
        btnYearCalendar = findViewById(R.id.btn_year_calendar);
        btnArrowRight = findViewById(R.id.btn_arrow_right);
        btnArrowLeft = findViewById(R.id.btn_arrow_left);
        layoutCalendarMonths = findViewById(R.id.layout_calendar_months);
        layoutJanuary = findViewById(R.id.layout_january);
        layoutFebruary = findViewById(R.id.layout_february);
        layoutMarch = findViewById(R.id.layout_march);
        layoutApril = findViewById(R.id.layout_april);
        layoutMay = findViewById(R.id.layout_may);
        layoutJune = findViewById(R.id.layout_june);
        layoutJuly = findViewById(R.id.layout_july);
        layoutAugust = findViewById(R.id.layout_august);
        layoutSeptember = findViewById(R.id.layout_september);
        layoutOctober = findViewById(R.id.layout_october);
        layoutNovember = findViewById(R.id.layout_november);
        layoutDecember = findViewById(R.id.layout_december);

        datesRecyclerView = findViewById(R.id.dates_list_recycler_view);
        obligationsRecyclerView = findViewById(R.id.obligations_list_recycler_view);

        contentView.setVisibility(View.VISIBLE);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalendarTemplateSecond.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(CalendarTemplateSecond.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalendarTemplateSecond.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalendarTemplateSecond.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalendarTemplateSecond.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutCalendarMonths.getVisibility() == View.GONE) {
                    layoutCalendarMonths.setVisibility(View.VISIBLE);
                } else {
                    layoutCalendarMonths.setVisibility(View.GONE);
                }
            }
        });

        btnYearCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutCalendarMonths.getVisibility() == View.GONE) {
                    layoutCalendarMonths.setVisibility(View.VISIBLE);
                } else {
                    layoutCalendarMonths.setVisibility(View.GONE);
                }
            }
        });

        layoutCalendarMonths.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutJanuary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 0;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutFebruary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 1;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutMarch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 2;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutApril.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 3;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutMay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 4;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutJune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 5;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutJuly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 6;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutAugust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 7;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutSeptember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 8;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutOctober.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 9;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutNovember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 10;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutDecember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenMonth = 11;
                setActiveCurrentMonth(true);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        btnArrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = Integer.parseInt(txtYear.getText().toString());
                year++;
                chosenYear = year;
                txtYear.setText(year + "");
                setActiveCurrentMonth(true);
            }
        });

        btnArrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = Integer.parseInt(txtYear.getText().toString());
                year--;
                txtYear.setText(year + "");
                chosenYear = year;
                setActiveCurrentMonth(true);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(CalendarTemplateSecond.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_SEARCH_TYPE);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void getCalendarDates() {
        httpService = new VolleyService(this, this, REQUEST_DATES_TYPE);
        datesList = new ArrayList<>();
        String params = "?date=" + (chosenMonth + 1) + "." + chosenYear + ".";
        httpService.get(HTTP_URL + params);
    }

    private void updateDatesListResultRecyclerView() {
        datesRecyclerView.setHasFixedSize(true);
        datesAdapter = new CalendarDatesAdapter(getApplicationContext(),
                datesList, currentDate, currentMonth, chosenMonth, currentYear, chosenYear,
                title, false, this);
        datesRecyclerView.setAdapter(datesAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        datesRecyclerView.setLayoutManager(layoutManager);
    }

    private void getCalendarObligations() {
        httpService = new VolleyService(this, this, REQUEST_OBLIGATIONS_TYPE);
        obligationsList = new ArrayList<>();
        String params = "?lang=sr&date=" + selectedDate + "." + (selectedMonth + 1) + "." + selectedYear + ".";
        httpService.get(HTTP_OBLIGATIONS_URL + params);
    }

    private void updateObligationsListResultRecyclerView() {
        obligationsRecyclerView.setHasFixedSize(true);
        CalendarObligationsAdapter obligationsAdapter = new CalendarObligationsAdapter(title,
                this, obligationsList);
        obligationsRecyclerView.setAdapter(obligationsAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        obligationsRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_SEARCH_TYPE) {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
            } else if (requestType == REQUEST_DATES_TYPE) {
                JSONArray entity = res.getJSONArray("entity");
                for (int i = 0; i < entity.length(); i++) {
                    boolean isSelected = false;
                    if (Integer.parseInt(entity.getString(i)) == selectedDate && chosenYear == selectedYear && chosenMonth == selectedMonth) {
                        isSelected = true;
                    }
                    if (isMonthChanged) {
                        isSelected = false;
                    }
                    datesList.add(new CalendarDate(Integer.parseInt(entity.getString(i)), isSelected));
                }

                updateDatesListResultRecyclerView();
            } else {
                JSONArray entity = res.getJSONArray("entity");
                for (int i = 0; i < entity.length(); i++) {
                    JSONObject item = entity.getJSONObject(i);
                    obligationsList.add(new CalendarObligation(item.getString("id"), item.getString("name"),
                            item.getString("date")));
                }

                updateObligationsListResultRecyclerView();
            }

            contentView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        } catch (JSONException e) {
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateChanged(int dateNumber) {
        if (CheckInternetConnection.isOnline(this)) {
            for (CalendarDate date : datesList) {
                date.setSelected(false);
                if (date.getDateNumber() == dateNumber) {
                    date.setSelected(true);
                }
            }

            selectedDate = dateNumber;
            selectedMonth = chosenMonth;
            selectedYear = chosenYear;
            isMonthChanged = false;

            datesAdapter.notifyDataSetChanged();
            getCalendarObligations();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    private void setActiveCurrentMonth(boolean isMonthChanged) {
        switch (chosenMonth) {
            case 0:
                setActive1();
                break;
            case 1:
                setActive2();
                break;
            case 2:
                setActive3();
                break;
            case 3:
                setActive4();
                break;
            case 4:
                setActive5();
                break;
            case 5:
                setActive6();
                break;
            case 6:
                setActive7();
                break;
            case 7:
                setActive8();
                break;
            case 8:
                setActive9();
                break;
            case 9:
                setActive10();
                break;
            case 10:
                setActive11();
                break;
            case 11:
                setActive12();
                break;
        }

        if (CheckInternetConnection.isOnline(this)) {
            this.isMonthChanged = isMonthChanged;
            getCalendarDates();
            obligationsList = new ArrayList<>();
            updateObligationsListResultRecyclerView();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    private void setActive1() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_january));
    }

    private void setActive2() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_february));
    }

    private void setActive3() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_march));
    }

    private void setActive4() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_april));
    }

    private void setActive5() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_may));
    }

    private void setActive6() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_june));
    }

    private void setActive7() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_july));
    }

    private void setActive8() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_august));
    }

    private void setActive9() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_september));
    }

    private void setActive10() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_october));
    }

    private void setActive11() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_november));
    }

    private void setActive12() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_december));
    }
}
