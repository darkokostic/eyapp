package rs.ey.taxserbia.entities.pages.types.templates;

import android.graphics.drawable.Drawable;

/**
 * Created by darko on 14.7.18..
 */

public class GridSubHeaderPageTemplate {
    private String id;
    private String objectId;
    private String title;
    private String imgUrl;
    private String type;

    public GridSubHeaderPageTemplate(String id, String objectId, String title, String imgUrl, String type) {
        this.id = id;
        this.objectId = objectId;
        this.title = title;
        this.imgUrl = imgUrl;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackgroundDrawable() {
        return imgUrl;
    }

    public void setBackgroundDrawable(Drawable backgroundDrawable) {
        this.imgUrl = imgUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
