package rs.ey.taxserbia;

import android.app.Application;

import com.onesignal.OneSignal;

import java.util.List;

import rs.ey.taxserbia.entities.settings.IndustryItem;
import rs.ey.taxserbia.notifications.NotificationOpenedHandler;

/**
 * Created by darko on 24.6.18..
 */

public class EyApplication extends Application {

    public static String chosenLanguage;
    public static String langShort;
    public static boolean cbAllStatus;
    public static boolean cbPushStatus;
    public static List<IndustryItem> chosenIndustries;
    public static final String[] POLL_ANSWERS_INDICATORS_SR = {
            "A", "Б", "В", "Г", "Д", "Ђ", "Е", "Ж", "З", "И", "Ј", "К", "Л", "Љ", "М", "Н",
            "Њ", "О", "П", "Р", "С", "Т", "Ћ", "У", "Ф", "Х", "Ц", "Ч", "Џ", "Ш"
    };
    public static final String[] POLL_ANSWERS_INDICATORS_EN = {
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    };

    @Override
    public void onCreate() {
        super.onCreate();

        // Onesignal Init
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .init();
    }
}
