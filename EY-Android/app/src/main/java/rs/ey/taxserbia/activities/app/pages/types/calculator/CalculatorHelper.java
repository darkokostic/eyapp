package rs.ey.taxserbia.activities.app.pages.types.calculator;

/**
 * Created by darko on 24.10.18..
 */

public class CalculatorHelper {

    public static final String RSD_VALUE = "RSD";
    public static final String EUR_VALUE = "EUR";

    public static final String B1_N_VALUE = "B1_N";
    public static final String B2_N_VALUE = "B2_N";
    public static final String N_B1_VALUE = "N_B1";
    public static final String N_B2_VALUE = "N_B2";
    public static final String N_B_VALUE = "N_B";
    public static final String B_N_VALUE = "B_N";
    public static final String N_VALUE = "N";
    public static final String B1_VALUE = "B1";
    public static final String B2_VALUE = "B2";

    public static final String ACTIVITY_EARNINGS = "CalculatorEarningsTemplate";
    public static final String ACTIVITY_CONTRACT = "CalculatorServiceContractTemplate";

    public static final int YES_ANSWER = 1;
    public static final int NO_ANSWER = 0;
}
