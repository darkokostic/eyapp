package rs.ey.taxserbia.adapters.page.types;

import rs.ey.taxserbia.entities.pages.types.templates.PollAnswerItem;

/**
 * Created by darko on 19.9.18..
 */

public interface PollAnswerItemClickHandler {

    void openPollAnswerClick(PollAnswerItem item);
}
