package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.entities.pages.types.templates.PollAnswerResultItem;

/**
 * Created by darko on 28.6.18..
 */

public class PollResultsAnswersAdapter extends RecyclerView.Adapter<PollResultsAnswersAdapter.PollResultsAnswersViewHolder> {

    private ArrayList<PollAnswerResultItem> data;
    private LayoutInflater inflater;
    private Context context;

    public PollResultsAnswersAdapter(Context context, ArrayList<PollAnswerResultItem> data) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public PollResultsAnswersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_answer_result_item_poll_list_template, parent, false);
        return new PollResultsAnswersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PollResultsAnswersViewHolder holder, final int position) {
        final PollAnswerResultItem pollAnswerResultItem = data.get(position);

        holder.txtAnswerPercentage.setText(pollAnswerResultItem.getPercentage() + "%");
        holder.txtOptionCount.setText(pollAnswerResultItem.getAnswerCountChar());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PollResultsAnswersViewHolder extends RecyclerView.ViewHolder {

        TextView txtAnswerPercentage;
        TextView txtOptionCount;

        public PollResultsAnswersViewHolder(View itemView) {
            super(itemView);
            txtAnswerPercentage = itemView.findViewById(R.id.txt_answer_percentage);
            txtOptionCount = itemView.findViewById(R.id.txt_answer_count);
        }
    }
}
