package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 1.10.18..
 */

public class TaxNewsItem {

    private String id;
    private String title;
    private String date;
    private String time;
    private String thumbnailUrl;

    public TaxNewsItem(String id, String title, String date, String time, String thumbnailUrl) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.time = time;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
