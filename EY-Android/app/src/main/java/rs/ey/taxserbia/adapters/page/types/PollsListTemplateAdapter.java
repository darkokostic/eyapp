package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.entities.pages.types.templates.PollItem;

/**
 * Created by darko on 28.6.18..
 */

public class PollsListTemplateAdapter extends RecyclerView.Adapter<PollsListTemplateAdapter.PollsListTemplateViewHolder> {

    private ArrayList<PollItem> data;
    private LayoutInflater inflater;
    private Context context;
    private PollsListItemClickHandler pollClickHandler;

    public PollsListTemplateAdapter(Context context, ArrayList<PollItem> data,
                                    PollsListItemClickHandler pollClickHandler) {
        this.data = data;
        this.context = context;
        this.pollClickHandler = pollClickHandler;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public PollsListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_polls_list_template, parent, false);
        return new PollsListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PollsListTemplateViewHolder holder, final int position) {
        final PollItem pollItem = data.get(position);

        holder.txtDate.setText(pollItem.getDate());
        holder.txtQuestion.setText(pollItem.getName());

        holder.layoutPollItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pollClickHandler.openPollSingleView(pollItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PollsListTemplateViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutPollItem;
        TextView txtDate;
        TextView txtQuestion;

        public PollsListTemplateViewHolder(View itemView) {
            super(itemView);
            layoutPollItem = itemView.findViewById(R.id.layout_poll_item);
            txtDate = itemView.findViewById(R.id.txt_date);
            txtQuestion = itemView.findViewById(R.id.txt_question);
        }
    }
}
