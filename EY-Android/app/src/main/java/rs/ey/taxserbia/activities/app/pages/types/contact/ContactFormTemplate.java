package rs.ey.taxserbia.activities.app.pages.types.contact;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

public class ContactFormTemplate extends AppCompatActivity implements Responsable {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    private EditText etName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etMsg;
    private LinearLayout btnSend;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/contacts";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";

    private final static int REQUEST_TYPE_SEARCH = 2;
    private final static int REQUEST_TYPE_BASIC = 1;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_contact_form);

        StatusBarUtil.setColor(ContactFormTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        loadingLayout.setVisibility(View.GONE);

        etName = findViewById(R.id.et_name);
        etLastName = findViewById(R.id.et_lastname);
        etEmail = findViewById(R.id.et_email);
        etMsg = findViewById(R.id.et_msg);
        btnSend = findViewById(R.id.btn_send);
        httpService = new VolleyService(this, this, REQUEST_TYPE_BASIC);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactFormTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(ContactFormTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactFormTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactFormTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactFormTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternetConnection.isOnline(ContactFormTemplate.this)) {
                    loadingLayout.setVisibility(View.VISIBLE);
                    if (!etName.getText().toString().equals("") ||
                            !etLastName.getText().toString().equals("") ||
                            !etEmail.getText().toString().equals("") ||
                            !etMsg.getText().toString().equals("")) {
                        if (isEmailValid(etEmail.getText().toString())) {
                            sendMessageToServer();
                        } else {
                            loadingLayout.setVisibility(View.GONE);
                            Toast.makeText(ContactFormTemplate.this, getResources().getString(R.string.contact_send_invalid_email_msg), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        loadingLayout.setVisibility(View.GONE);
                        Toast.makeText(ContactFormTemplate.this, getResources().getString(R.string.contact_send_empty_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loadingLayout.setVisibility(View.GONE);
                    Toast.makeText(ContactFormTemplate.this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.home_header_contact_active));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                finish();
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void sendMessageToServer() {
        httpService = new VolleyService(this, this, REQUEST_TYPE_BASIC);
        JSONObject params = new JSONObject();
        try {
            params.put("first_name", etName.getText().toString());
            params.put("last_name", etLastName.getText().toString());
            params.put("email", etEmail.getText().toString());
            params.put("lang", EyApplication.langShort);
            params.put("message", etMsg.getText().toString());

            httpService.post(HTTP_URL, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void emptyInputs() {
        etName.setText("");
        etLastName.setText("");
        etEmail.setText("");
        etMsg.setText("");
    }

    private boolean isEmailValid(String email) {
        return VALID_EMAIL_ADDRESS_REGEX.matcher(email).find();
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_TYPE_BASIC) {
                loadingLayout.setVisibility(View.GONE);
                Toast.makeText(ContactFormTemplate.this, getResources().getString(R.string.contact_send_success_msg), Toast.LENGTH_SHORT).show();
                emptyInputs();
            } else {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
