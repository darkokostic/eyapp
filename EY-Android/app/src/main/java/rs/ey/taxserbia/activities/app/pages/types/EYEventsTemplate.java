package rs.ey.taxserbia.activities.app.pages.types;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.EYEventsListTemplateAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.EYEventItem;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.adapters.page.types.EYEventsListTemplateAdapter.VIEW_TYPE_ACTIVE;
import static rs.ey.taxserbia.adapters.page.types.EYEventsListTemplateAdapter.VIEW_TYPE_PAST;

public class EYEventsTemplate extends AppCompatActivity implements Responsable {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;
    private String title;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/news/events";
    private final static String HTTP_FILTERED_EVENTS_URL = "https://ey.nbgcreator.com/api/news/filter_events";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private final static int REQUEST_EVENTS_LIST_TYPE = 1;
    private final static int REQUEST_FILTERED_EVENTS_LIST_TYPE = 2;
    private final static int REQUEST_TYPE_SEARCH = 3;

    private LinearLayout contentView;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    private RecyclerView eyEventListRecyclerView;
    private RecyclerView eyActiveEventListRecyclerView;
    private RecyclerView eyCompletedEventListRecyclerView;
    private ArrayList<EYEventItem> eyEventList;
    private ArrayList<EYEventItem> eyActiveEventList;
    private ArrayList<EYEventItem> eyCompletedEventList;
    private ImageView btnCalendar;
    private TextView txtYear;
    private TextView txtMonthName;
    private LinearLayout btnYearCalendar;
    private LinearLayout btnArrowRight;
    private LinearLayout btnArrowLeft;
    private ScrollView layoutCalendarMonths;
    private LinearLayout layoutJanuary;
    private LinearLayout layoutFebruary;
    private LinearLayout layoutMarch;
    private LinearLayout layoutApril;
    private LinearLayout layoutMay;
    private LinearLayout layoutJune;
    private LinearLayout layoutJuly;
    private LinearLayout layoutAugust;
    private LinearLayout layoutSeptember;
    private LinearLayout layoutOctober;
    private LinearLayout layoutNovember;
    private LinearLayout layoutDecember;
    private int currentYear;
    private int currentMonth;
    private int choosenYear;
    private LinearLayout layoutFilteredEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_ey_events);

        title = getIntent().getExtras().getString("title");

        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        choosenYear = currentYear;
        currentMonth = Calendar.getInstance().get(Calendar.MONTH);

        eyEventListRecyclerView = findViewById(R.id.events_list_recycler_view);
        eyActiveEventListRecyclerView = findViewById(R.id.events_list_active_recycler_view);
        eyCompletedEventListRecyclerView = findViewById(R.id.events_list_completed_recycler_view);

        StatusBarUtil.setColor(EYEventsTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (CheckInternetConnection.isOnline(this)) {
            requestEventsList();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    @SuppressLint("SetTextI18n")
    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        contentView = findViewById(R.id.content_view);
        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        txtMonthName = findViewById(R.id.txt_month_name);
        txtYear = findViewById(R.id.txt_year);
        txtYear.setText(Integer.toString(currentYear));
        txtMonthName.setVisibility(View.GONE);

        btnCalendar = findViewById(R.id.btn_calendar);
        btnYearCalendar = findViewById(R.id.btn_year_calendar);
        btnArrowRight = findViewById(R.id.btn_arrow_right);
        btnArrowLeft = findViewById(R.id.btn_arrow_left);
        layoutCalendarMonths = findViewById(R.id.layout_calendar_months);
        layoutJanuary = findViewById(R.id.layout_january);
        layoutFebruary = findViewById(R.id.layout_february);
        layoutMarch = findViewById(R.id.layout_march);
        layoutApril = findViewById(R.id.layout_april);
        layoutMay = findViewById(R.id.layout_may);
        layoutJune = findViewById(R.id.layout_june);
        layoutJuly = findViewById(R.id.layout_july);
        layoutAugust = findViewById(R.id.layout_august);
        layoutSeptember = findViewById(R.id.layout_september);
        layoutOctober = findViewById(R.id.layout_october);
        layoutNovember = findViewById(R.id.layout_november);
        layoutDecember = findViewById(R.id.layout_december);
        layoutFilteredEvents = findViewById(R.id.layout_filtered_events);

        contentView.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);
        layoutCalendarMonths.setVisibility(View.GONE);
        layoutFilteredEvents.setVisibility(View.GONE);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EYEventsTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(EYEventsTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EYEventsTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EYEventsTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EYEventsTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutCalendarMonths.getVisibility() == View.GONE) {
                    layoutCalendarMonths.setVisibility(View.VISIBLE);
                } else {
                    layoutCalendarMonths.setVisibility(View.GONE);
                }
            }
        });

        btnYearCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutCalendarMonths.getVisibility() == View.GONE) {
                    layoutCalendarMonths.setVisibility(View.VISIBLE);
                } else {
                    layoutCalendarMonths.setVisibility(View.GONE);
                }
            }
        });

        layoutCalendarMonths.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutJanuary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 0;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutFebruary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 1;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutMarch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 2;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutApril.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 3;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutMay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 4;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutJune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 5;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutJuly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 6;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutAugust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 7;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutSeptember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 8;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutOctober.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 9;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutNovember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 10;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        layoutDecember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMonth = 11;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
                layoutCalendarMonths.setVisibility(View.GONE);
            }
        });

        btnArrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = Integer.parseInt(txtYear.getText().toString());
                year++;
                choosenYear = year;
                txtYear.setText(year + "");
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
            }
        });

        btnArrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = Integer.parseInt(txtYear.getText().toString());
                year--;
                txtYear.setText(year + "");
                choosenYear = year;
                setActiveCurrentMonth();
                requestFilteredList(choosenYear);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(EYEventsTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void createEvent(ArrayList<EYEventItem> eyEventList, String id, String thumbnailUrl, String publishDate, String startDate, String endDate, String title) {
        EYEventItem eventItem = new EYEventItem(id, thumbnailUrl, publishDate, startDate, endDate, title);
        eyEventList.add(eventItem);
    }

    private void requestEventsList() {
        httpService = new VolleyService(this, this, REQUEST_EVENTS_LIST_TYPE);
        eyEventList = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort;
        httpService.get(HTTP_URL + params);
    }

    private void requestFilteredList(int year) {
        eyEventListRecyclerView.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);
        layoutFilteredEvents.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_FILTERED_EVENTS_LIST_TYPE);
        eyActiveEventList = new ArrayList<>();
        eyCompletedEventList = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort
                + "&date=" + year + "-" + (currentMonth + 1);
        httpService.get(HTTP_FILTERED_EVENTS_URL + params);
    }

    private void createListLayout() {
        eyEventListRecyclerView.setHasFixedSize(true);
        final EYEventsListTemplateAdapter eyEventsListTemplateAdapter = new EYEventsListTemplateAdapter(title, this, eyEventList, VIEW_TYPE_ACTIVE);
        eyEventListRecyclerView.setAdapter(eyEventsListTemplateAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        eyEventListRecyclerView.setLayoutManager(layoutManager);
    }

    private void createActiveListLayout() {
        eyActiveEventListRecyclerView.setHasFixedSize(true);
        final EYEventsListTemplateAdapter eyEventsListTemplateAdapter = new EYEventsListTemplateAdapter(title, this, eyActiveEventList, VIEW_TYPE_ACTIVE);
        eyActiveEventListRecyclerView.setAdapter(eyEventsListTemplateAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        eyActiveEventListRecyclerView.setLayoutManager(layoutManager);
    }

    private void createCompletedEventsListLayout() {
        eyCompletedEventListRecyclerView.setHasFixedSize(true);
        final EYEventsListTemplateAdapter eyEventsListTemplateAdapter = new EYEventsListTemplateAdapter(title, this, eyCompletedEventList, VIEW_TYPE_PAST);
        eyCompletedEventListRecyclerView.setAdapter(eyEventsListTemplateAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        eyCompletedEventListRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_EVENTS_LIST_TYPE) {
                JSONArray entity = res.getJSONArray("entity");
                for (int i = 0; i < entity.length(); i++) {
                    JSONObject page = entity.getJSONObject(i);
                    createEvent(eyEventList, page.getString("id"), page.getString("thumbnail"),
                            page.getString("publish_date"), page.getString("start_date"),
                            page.getString("end_date"), page.getString("title"));
                }

                createListLayout();
            } else if (requestType == REQUEST_FILTERED_EVENTS_LIST_TYPE) {
                JSONObject entity = res.getJSONObject("entity");

                JSONArray activeEvents = entity.getJSONArray("active");
                for (int i = 0; i < activeEvents.length(); i++) {
                    JSONObject page = activeEvents.getJSONObject(i);
                    createEvent(eyActiveEventList, page.getString("id"), page.getString("thumbnail"),
                            page.getString("publish_date"), page.getString("start_date"),
                            page.getString("end_date"), page.getString("title"));
                }

                createActiveListLayout();

                JSONArray completedEvents = entity.getJSONArray("completed");
                for (int i = 0; i < completedEvents.length(); i++) {
                    JSONObject page = completedEvents.getJSONObject(i);
                    createEvent(eyCompletedEventList, page.getString("id"), page.getString("thumbnail"),
                            page.getString("publish_date"), page.getString("start_date"),
                            page.getString("end_date"), page.getString("title"));
                }

                createCompletedEventsListLayout();
            } else {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
            }

            contentView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        } catch (JSONException e) {
            loadingLayout.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setActiveCurrentMonth() {
        switch (currentMonth) {
            case 0:
                setActive1();
                break;
            case 1:
                setActive2();
                break;
            case 2:
                setActive3();
                break;
            case 3:
                setActive4();
                break;
            case 4:
                setActive5();
                break;
            case 5:
                setActive6();
                break;
            case 6:
                setActive7();
                break;
            case 7:
                setActive8();
                break;
            case 8:
                setActive9();
                break;
            case 9:
                setActive10();
                break;
            case 10:
                setActive11();
                break;
            case 11:
                setActive12();
                break;
        }
        txtMonthName.setVisibility(View.VISIBLE);
    }

    private void setActive1() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_january));
    }

    private void setActive2() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_february));
    }

    private void setActive3() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_march));
    }

    private void setActive4() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_april));
    }

    private void setActive5() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_may));
    }

    private void setActive6() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_june));
    }

    private void setActive7() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_july));
    }

    private void setActive8() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_august));
    }

    private void setActive9() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_september));
    }

    private void setActive10() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_october));
    }

    private void setActive11() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_november));
    }

    private void setActive12() {
        layoutJanuary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutFebruary.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMarch.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutApril.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutMay.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJune.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutJuly.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutAugust.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutSeptember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutOctober.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutNovember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_bg));
        layoutDecember.setBackground(getResources().getDrawable(R.drawable.ey_events_calendar_months_active_bg));
        txtMonthName.setText(getResources().getString(R.string.ey_events_december));
    }
}
