package rs.ey.taxserbia.activities.intro.steps;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesignal.OneSignal;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.home.pages.ChooseIndustriesFragment;
import rs.ey.taxserbia.services.LocalStorage;

@SuppressWarnings("NullableProblems")
public class IntroCurrentSettingsFragment extends Fragment {

    public IntroCurrentSettingsFragment() {}

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro_current_settings, container, false);

        TextView txtChoosedLanguage = view.findViewById(R.id.txtChoosedLanguage);
        TextView txtChoosedIndustriesNumber = view.findViewById(R.id.txtChoosedIndustriesNumber);
        final CheckBox checkBox = view.findViewById(R.id.checkBox);
        LinearLayout cbPushLayout = view.findViewById(R.id.cbPush);
        LinearLayout btnChooseIndustries = view.findViewById(R.id.btnChooseIndustries);
        final FrameLayout layoutChooseIndustries = getActivity().findViewById(R.id.choose_industries_layout);

        txtChoosedLanguage.setText(EyApplication.chosenLanguage);
        txtChoosedIndustriesNumber.setText("(" + EyApplication.chosenIndustries.size() + ")");

        checkBox.setChecked(EyApplication.cbPushStatus);
        cbPushLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setChecked(!checkBox.isChecked());
                EyApplication.cbPushStatus = checkBox.isChecked();
                LocalStorage.setPushNotificationsStatus(getContext(), EyApplication.cbPushStatus);
                OneSignal.setSubscription(EyApplication.cbPushStatus);
            }
        });

        btnChooseIndustries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.choose_industries_layout, new ChooseIndustriesFragment())
                        .commit();
                layoutChooseIndustries.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}
