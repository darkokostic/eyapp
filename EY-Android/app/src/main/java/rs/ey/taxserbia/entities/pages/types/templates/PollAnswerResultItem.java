package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 19.9.18..
 */

public class PollAnswerResultItem {

    private String id;
    private String percentage;
    private String answerCountChar;

    public PollAnswerResultItem(String id, String percentage, String answerCountChar) {
        this.id = id;
        this.percentage = percentage;
        this.answerCountChar = answerCountChar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getAnswerCountChar() {
        return answerCountChar;
    }

    public void setAnswerCountChar(String answerCountChar) {
        this.answerCountChar = answerCountChar;
    }
}
