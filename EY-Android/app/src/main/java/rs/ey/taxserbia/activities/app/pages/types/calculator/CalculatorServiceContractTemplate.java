package rs.ey.taxserbia.activities.app.pages.types.calculator;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.ACTIVITY_CONTRACT;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.EUR_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.NO_ANSWER;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.RSD_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.YES_ANSWER;

public class CalculatorServiceContractTemplate extends AppCompatActivity implements Responsable {

    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private VolleyService httpService;

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private String title;

    private LinearLayout card1;
    private LinearLayout card2;
    private LinearLayout card3;
    private LinearLayout card1Option1Active;
    private LinearLayout card1Option1Inactive;
    private LinearLayout card1Option2Active;
    private LinearLayout card1Option2Inactive;
    private LinearLayout card2Option1Active;
    private LinearLayout card2Option1Inactive;
    private LinearLayout card2Option2Active;
    private LinearLayout card2Option2Inactive;
    private LinearLayout card3Option1Active;
    private LinearLayout card3Option1Inactive;
    private LinearLayout card3Option2Active;
    private LinearLayout card3Option2Inactive;

    private String card1Option;
    private String card2Option;
    private String card3Option;
    private int securityAnswer;

    private TextView txtCard1Option1;
    private TextView txtCard1Option2;
    private TextView txtCard2Option1;
    private TextView txtCard2Option2;
    private TextView txtCard3Option1;
    private TextView txtCard3Option2;

    private LinearLayout btnContinue;
    private String calculation;
    private String calculationValute;

    private ImageView btnInfo;
    private LinearLayout layoutInfo;
    private LinearLayout btnCloseInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_calculator_service_contract);

        title = getIntent().getExtras().getString("title");

        StatusBarUtil.setColor(CalculatorServiceContractTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        btnInfo = findViewById(R.id.btn_info);
        layoutInfo = findViewById(R.id.layout_info);
        btnCloseInfo = findViewById(R.id.btn_close_info);

        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        card1 = findViewById(R.id.card_first);
        card2 = findViewById(R.id.card_second);
        card3 = findViewById(R.id.card_third);

        card1Option1Active = findViewById(R.id.card_1_option_1_active);
        card1Option1Inactive = findViewById(R.id.card_1_option_1_inactive);
        card1Option2Active = findViewById(R.id.card_1_option_2_active);
        card1Option2Inactive = findViewById(R.id.card_1_option_2_inactive);
        card2Option1Active = findViewById(R.id.card_2_option_1_active);
        card2Option1Inactive = findViewById(R.id.card_2_option_1_inactive);
        card2Option2Active = findViewById(R.id.card_2_option_2_active);
        card2Option2Inactive = findViewById(R.id.card_2_option_2_inactive);
        card3Option1Active = findViewById(R.id.card_3_option_1_active);
        card3Option1Inactive = findViewById(R.id.card_3_option_1_inactive);
        card3Option2Active = findViewById(R.id.card_3_option_2_active);
        card3Option2Inactive = findViewById(R.id.card_3_option_2_inactive);

        txtCard1Option1 = findViewById(R.id.txt_card_1_option_1);
        txtCard1Option2 = findViewById(R.id.txt_card_1_option_2);
        txtCard2Option1 = findViewById(R.id.txt_card_2_option_1);
        txtCard2Option2 = findViewById(R.id.txt_card_2_option_2);
        txtCard3Option1 = findViewById(R.id.txt_card_3_option_1);
        txtCard3Option2 = findViewById(R.id.txt_card_3_option_2);

        btnContinue = findViewById(R.id.btn_continue);

        httpService = new VolleyService(this, this);

        initRadioButtonsValues();
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorServiceContractTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(CalculatorServiceContractTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorServiceContractTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorServiceContractTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorServiceContractTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        card1Option1Inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCard1Active();
                setCard1Active1();
            }
        });

        card1Option2Inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCard1Active();
                setCard1Active2();
            }
        });

        card2Option1Inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCard2Active();
                setCard2Active1();
            }
        });

        card2Option2Inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCard2Active();
                setCard2Active2();
            }
        });

        card3Option1Inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCard3Active();
                setCard3Active1();
            }
        });

        card3Option2Inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCard3Active();
                setCard3Active2();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CalculatorServiceContractTemplate.this, CalculatorInputTemplate.class);
                intent.putExtra("option1", card1Option);
                intent.putExtra("option2", card2Option);
                intent.putExtra("option3", card3Option);
                intent.putExtra("activityName", ACTIVITY_CONTRACT);
                intent.putExtra("calculationValute", calculationValute);
                intent.putExtra("calculation", calculation);
                intent.putExtra("title", title);
                intent.putExtra("securityAnswer", securityAnswer);
                startActivity(intent);
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutInfo.setVisibility(View.VISIBLE);
            }
        });

        btnCloseInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutInfo.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(CalculatorServiceContractTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            JSONArray entity = res.getJSONArray("entity");

            for (int i = 0; i < entity.length(); i++) {
                JSONObject searchItem = entity.getJSONObject(i);
                createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
            }

            updateSearchListResultRecyclerView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        try {
            Toast.makeText(this, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initRadioButtonsValues() {
        setCard1Active1();
        setCard2Active1();
        setCard3Active1();
    }

    private void setCard1Active() {
        card1.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_active_bg));
        card2.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_inactive_bg));
        card3.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_inactive_bg));
    }

    private void setCard2Active() {
        card1.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_inactive_bg));
        card2.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_active_bg));
        card3.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_inactive_bg));
    }

    private void setCard3Active() {
        card1.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_inactive_bg));
        card2.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_inactive_bg));
        card3.setBackground(getResources().getDrawable(R.drawable.calculator_square_item_active_bg));
    }

    private void setCard1Active1() {
        card1Option1Active.setVisibility(View.VISIBLE);
        card1Option1Inactive.setVisibility(View.GONE);
        card1Option2Active.setVisibility(View.GONE);
        card1Option2Inactive.setVisibility(View.VISIBLE);
        card1Option = txtCard1Option1.getText().toString();
        calculationValute = RSD_VALUE;
    }

    private void setCard1Active2() {
        card1Option1Active.setVisibility(View.GONE);
        card1Option1Inactive.setVisibility(View.VISIBLE);
        card1Option2Active.setVisibility(View.VISIBLE);
        card1Option2Inactive.setVisibility(View.GONE);
        card1Option = txtCard1Option2.getText().toString();
        calculationValute = EUR_VALUE;
    }

    private void setCard2Active1() {
        card2Option1Active.setVisibility(View.VISIBLE);
        card2Option1Inactive.setVisibility(View.GONE);
        card2Option2Active.setVisibility(View.GONE);
        card2Option2Inactive.setVisibility(View.VISIBLE);
        card2Option = getResources().getString(R.string.calculator_service_option2_value1);
        calculation = N_B_VALUE;
    }

    private void setCard2Active2() {
        card2Option1Active.setVisibility(View.GONE);
        card2Option1Inactive.setVisibility(View.VISIBLE);
        card2Option2Active.setVisibility(View.VISIBLE);
        card2Option2Inactive.setVisibility(View.GONE);
        card2Option = getResources().getString(R.string.calculator_service_option2_value2);
        calculation = B_N_VALUE;
    }

    private void setCard3Active1() {
        card3Option1Active.setVisibility(View.VISIBLE);
        card3Option1Inactive.setVisibility(View.GONE);
        card3Option2Active.setVisibility(View.GONE);
        card3Option2Inactive.setVisibility(View.VISIBLE);
        card3Option = txtCard3Option1.getText().toString();
        securityAnswer = YES_ANSWER;
    }

    private void setCard3Active2() {
        card3Option1Active.setVisibility(View.GONE);
        card3Option1Inactive.setVisibility(View.VISIBLE);
        card3Option2Active.setVisibility(View.VISIBLE);
        card3Option2Inactive.setVisibility(View.GONE);
        card3Option = txtCard3Option2.getText().toString();
        securityAnswer = NO_ANSWER;
    }
}
