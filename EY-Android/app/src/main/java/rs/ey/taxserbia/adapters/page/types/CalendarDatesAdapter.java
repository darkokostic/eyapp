package rs.ey.taxserbia.adapters.page.types;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplateSecond;
import rs.ey.taxserbia.entities.pages.types.templates.CalendarDate;
import rs.ey.taxserbia.services.calendar.CalendarDateClickable;

public class CalendarDatesAdapter extends RecyclerView.Adapter<CalendarDatesAdapter.CalendarDatesViewHolder> {

    private Context context;
    private ArrayList<CalendarDate> data;
    private LayoutInflater inflater;
    public static final int PASSED_DATE_LAYOUT = 1;
    public static final int CURRENT_DATE_LAYOUT = 2;
    public static final int FUTURE_DATE_LAYOUT = 3;
    private int currentDate;
    private int currentMonth;
    private int chosenMonth;
    private int currentYear;
    private int chosenYear;
    private String title;
    private boolean isFirstTemplate;
    private CalendarDateClickable dateClickable;

    public CalendarDatesAdapter(Context context, ArrayList<CalendarDate> data, int currentDate,
                                int currentMonth, int chosenMonth, int currentYear,
                                int chosenYear, String title, boolean isFirstTemplate, CalendarDateClickable dateClickable) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.currentDate = currentDate;
        this.currentMonth = currentMonth;
        this.chosenMonth = chosenMonth;
        this.currentYear = currentYear;
        this.chosenYear = chosenYear;
        this.title = title;
        this.isFirstTemplate = isFirstTemplate;
        this.dateClickable = dateClickable;
    }

    @Override
    public CalendarDatesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == CURRENT_DATE_LAYOUT) {
            view = inflater.inflate(R.layout.card_calendar_date_item_current, parent, false);
        } else if (viewType == PASSED_DATE_LAYOUT) {
            view = inflater.inflate(R.layout.card_calendar_date_item_passed, parent, false);
        } else {
            view = inflater.inflate(R.layout.card_calendar_date_item_future, parent, false);
        }

        return new CalendarDatesViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        CalendarDate calendarDate = data.get(position);
        int date = calendarDate.getDateNumber();

        if (calendarDate.isSelected()) {
            return CURRENT_DATE_LAYOUT;
        }

        if (chosenYear < currentYear) {
            return PASSED_DATE_LAYOUT;
        }

        if (chosenYear > currentYear) {
            return FUTURE_DATE_LAYOUT;
        }

        if (chosenMonth < currentMonth) {
            return PASSED_DATE_LAYOUT;
        }

        if (chosenMonth > currentMonth) {
            return FUTURE_DATE_LAYOUT;
        }

        if (date < currentDate) {
            return PASSED_DATE_LAYOUT;
        } else {
            return FUTURE_DATE_LAYOUT;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(CalendarDatesViewHolder holder, final int position) {
        final CalendarDate calendarDate = data.get(position);
        holder.txtDate.setText(calendarDate.getDateNumber() + "");

        if (isFirstTemplate) {
            holder.layoutDateItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CalendarTemplateSecond.class);
                    intent.putExtra("title", title);
                    intent.putExtra("chosenDate", calendarDate.getDateNumber());
                    intent.putExtra("chosenYear", chosenYear);
                    intent.putExtra("chosenMonth", chosenMonth);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        } else {
            holder.layoutDateItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dateClickable.onDateChanged(calendarDate.getDateNumber());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CalendarDatesViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutDateItem;
        TextView txtDate;

        public CalendarDatesViewHolder(View itemView) {
            super(itemView);
            layoutDateItem = itemView.findViewById(R.id.layout_date_item);
            txtDate = itemView.findViewById(R.id.txt_date);
        }
    }
}
