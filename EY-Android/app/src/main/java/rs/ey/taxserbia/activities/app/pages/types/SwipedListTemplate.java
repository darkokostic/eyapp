package rs.ey.taxserbia.activities.app.pages.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gtomato.android.ui.transformer.CoverFlowViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;
import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.SwipedListTemplateAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.TaxNewsItem;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

public class SwipedListTemplate extends AppCompatActivity implements Responsable {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/news/tax_newsletter";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";

    private final static int REQUEST_TYPE_SEARCH = 2;
    private final static int REQUEST_TYPE_BASIC = 1;

    private LinearLayout contentView;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    private RecyclerView taxListRecyclerView;
    private TextView txtTitle;
    ArrayList<TaxNewsItem> taxList;

    private String title;

    private LinearLayout layoutSort;
    private ImageView btnSort;
    private LinearLayout layoutSortItemsActive;
    private LinearLayout btnSortActive;
    private FrameLayout btnSortNewest;
    private FrameLayout btnSortRead;

    private CarouselView carousel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_swiped_list);

        title = getIntent().getExtras().getString("title");

        StatusBarUtil.setColor(SwipedListTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (CheckInternetConnection.isOnline(this)) {
            requestTaxList("new");
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        taxListRecyclerView = findViewById(R.id.tax_list_recycler_view);

        contentView = findViewById(R.id.content_view);
        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        contentView.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);

        layoutSort = findViewById(R.id.layout_sort);
        btnSort = findViewById(R.id.btn_sort);
        layoutSortItemsActive = findViewById(R.id.layout_sort_items_active);
        btnSortActive = findViewById(R.id.btn_sort_active);
        btnSortNewest = findViewById(R.id.btn_sort_newest);
        btnSortRead = findViewById(R.id.btn_sort_read);

        carousel = findViewById(R.id.carousel);

        txtTitle = findViewById(R.id.txt_title);
        if (!title.equals("Notification")) {
            txtTitle.setText(title);
        }
        httpService = new VolleyService(this, this, REQUEST_TYPE_BASIC);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SwipedListTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!title.equals("Notification")) {
                    finish();
                } else {
                    startActivity(new Intent(SwipedListTemplate.this, HomeActivity.class));
                    finish();
                }
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(SwipedListTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SwipedListTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SwipedListTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SwipedListTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSort.setVisibility(View.VISIBLE);
            }
        });

        layoutSortItemsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSort.setVisibility(View.GONE);
            }
        });

        btnSortActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSort.setVisibility(View.GONE);
            }
        });

        btnSortNewest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternetConnection.isOnline(getApplicationContext())) {
                    requestTaxList("new");
                } else {
                    loadingLayout.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                }
                layoutSort.setVisibility(View.GONE);
            }
        });

        btnSortRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternetConnection.isOnline(getApplicationContext())) {
                    requestTaxList("read");
                } else {
                    loadingLayout.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                }
                layoutSort.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(SwipedListTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void createListLayout() {
        taxListRecyclerView.setHasFixedSize(true);
        final SwipedListTemplateAdapter swipedListTemplateAdapter = new SwipedListTemplateAdapter(title, this, this, taxList);
        taxListRecyclerView.setAdapter(swipedListTemplateAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        taxListRecyclerView.setLayoutManager(layoutManager);

        carousel.setTransformer(new CoverFlowViewTransformer());
        carousel.setAdapter(swipedListTemplateAdapter);

        carousel.setOnItemClickListener(new CarouselView.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView.Adapter adapter, View view, int position, int adapterPosition) {
                if (position == carousel.getCurrentPosition()) {
                    TaxNewsItem taxNewsItem = taxList.get(position);
                    Intent intent = new Intent(SwipedListTemplate.this, SwipedListSingleViewTemplate.class);
                    intent.putExtra("id", taxNewsItem.getId());
                    intent.putExtra("title", title);
                    startActivity(intent);
                }
            }
        });
    }

    private void createTaxItem(ArrayList<TaxNewsItem> taxNewsItems, String id, String title, String date, String time, String coverImageUrl) {
        TaxNewsItem newsItem = new TaxNewsItem(id, title, date, time, coverImageUrl);
        taxNewsItems.add(newsItem);
    }

    private void requestTaxList(String sort) {
        taxList = new ArrayList<>();
        String params = "?lang="
                + EyApplication.langShort
                + "&sort=" + sort;
        httpService.get(HTTP_URL + params);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_TYPE_BASIC) {
                JSONArray taxList = res.getJSONArray("entity");
                for (int i = 0; i < taxList.length(); i++) {
                    JSONObject newsItem = taxList.getJSONObject(i);
                    String dateTime = newsItem.getString("publish_date");
                    createTaxItem(this.taxList, newsItem.getString("id"), newsItem.getString("title"), dateTime.split(" ")[0], dateTime.split(" ")[1], null);
                }

                createListLayout();
            } else {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
            }

            contentView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
