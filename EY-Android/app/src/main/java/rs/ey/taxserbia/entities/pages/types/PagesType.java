package rs.ey.taxserbia.entities.pages.types;

/**
 * Created by darko on 17.7.18..
 */

public class PagesType {
    public static final String GRID_BG = "grid_bg";
    public static final String GRID_ICON = "grid_icon";
    public static final String NEWS_LIST = "news_list";
    public static final String ARTICLE = "article";
    public static final String EXPANDABLE_LIST = "expandable_list";
    public static final String VIDEO_LIST = "video_list";
    public static final String POLL_LIST = "poll_list";
    public static final String EVENT_LIST = "event_list";
    public static final String SWIPED_LIST = "swiped_list";
    public static final String INDUSTRY_OVERVIEW = "industry_overview";
    public static final String INDUSTRY_OVERVIEW_GRID = "industry_overview_grid";
    public static final String CALCULATOR_EARNINGS = "calculator_earnings";
    public static final String CALCULATOR_SERVICE_CONTRACT = "calculator_service_contract";
    public static final String CALCULATOR_YEAR_TAX = "calculator_annual_tax";
    public static final String PAGE_VIEW = "page_view";
    public static final String POLL_SINGLE = "poll";
    public static final String POLL_RESULTS = "poll_results";
    public static final String CALENDAR_TEMPLATE = "calendar";
    public static final String GRID_BG_CONTACT_TEMPLATE = "grid_bg_contact";
}
