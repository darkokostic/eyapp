package rs.ey.taxserbia.notifications;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.NewsListSingleViewTemplate;
import rs.ey.taxserbia.activities.app.pages.types.OnlyImagesTemplate;
import rs.ey.taxserbia.activities.app.pages.types.PollResultsTemplate;
import rs.ey.taxserbia.activities.app.pages.types.PollSingleViewTemplate;
import rs.ey.taxserbia.activities.app.pages.types.SwipedListTemplate;

import static rs.ey.taxserbia.entities.pages.types.PagesType.ARTICLE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EXPANDABLE_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.PAGE_VIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.POLL_RESULTS;
import static rs.ey.taxserbia.entities.pages.types.PagesType.POLL_SINGLE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.SWIPED_LIST;

/**
 * Created by darko on 18.10.18..
 */

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    private Context context;

    public NotificationOpenedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        JSONObject additionalData = result.notification.payload.additionalData;
        try {
            String template = additionalData.getString("template");
            String objectId = additionalData.getString("object_id");

            Intent intent;
            switch (template) {
                case ARTICLE:
                    intent = new Intent(context, NewsListSingleViewTemplate.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", objectId);
                    intent.putExtra("title", "Notification");
                    context.startActivity(intent);
                    break;
                case SWIPED_LIST:
                    intent = new Intent(context, SwipedListTemplate.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("title", "Notification");
                    context.startActivity(intent);
                    break;
                case PAGE_VIEW:
                    intent = new Intent(context, OnlyImagesTemplate.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", objectId);
                    intent.putExtra("title", "Notification");
                    context.startActivity(intent);
                    break;
                case EXPANDABLE_LIST:
                    intent = new Intent(context, ExpandableListTemplate.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", objectId);
                    intent.putExtra("title", "Notification");
                    context.startActivity(intent);
                    break;
                case POLL_SINGLE:
                    intent = new Intent(context, PollSingleViewTemplate.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", objectId);
                    intent.putExtra("title", "Notification");
                    context.startActivity(intent);
                    break;
                case POLL_RESULTS:
                    intent = new Intent(context, PollResultsTemplate.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", objectId);
                    intent.putExtra("title", "Notification");
                    context.startActivity(intent);
                    break;
                    default:
                        intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        break;
            }
        } catch (Exception e) {
            String launchURL = result.notification.payload.launchURL;
            if (launchURL != null) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(launchURL));
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(browserIntent);
            } else {
                Intent intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }
    }
}
