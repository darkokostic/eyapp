package rs.ey.taxserbia.services.http;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by darko on 2.8.18..
 */

public class VolleyService {

    private Responsable respCallback;
    private Context context;
    private int requestType;
    private int repeatCount = 2;

    public VolleyService(Responsable respCallback, Context context) {
        this.respCallback = respCallback;
        this.context = context;
        this.requestType = 0;
    }

    public VolleyService(Responsable respCallback, Context context, int requestType) {
        this.respCallback = respCallback;
        this.context = context;
        this.requestType = requestType;
    }

    public void post(String url, JSONObject params) {
        try {
            RequestQueue que = Volley.newRequestQueue(context);
            Request request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    respCallback.onSuccessResponse(response, requestType);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        if(error.networkResponse.statusCode != 500) {
                            JSONObject errData = new JSONObject(Arrays.toString(error.networkResponse.data));
                            respCallback.onErrorResponse(errData);
                        } else {
                            JSONObject errData = new JSONObject();
                            errData.put("message", "Something went wrong with server");
                            respCallback.onErrorResponse(errData);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            que.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void get(final String url) {
        Log.i("URL", url);
        try {
            RequestQueue que = Volley.newRequestQueue(context);
            Request request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    repeatCount = 2;
                    respCallback.onSuccessResponse(response, requestType);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (repeatCount > 0) {
                        repeatCount--;
                        get(url);
                        return;
                    }
                    try {
                        if(error.networkResponse.statusCode != 500) {
                            JSONObject errData = new JSONObject(Arrays.toString(error.networkResponse.data));
                            respCallback.onErrorResponse(errData);
                        } else {
                            JSONObject errData = new JSONObject();
                            errData.put("message", "Something went wrong with server");
                            respCallback.onErrorResponse(errData);
                        }
                    } catch (JSONException | NullPointerException e) {
                        JSONObject errData = new JSONObject();
                        try {
                            errData.put("message", "Something went wrong with server");
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        respCallback.onErrorResponse(errData);
                    }
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(
                    1000,
                    5,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            que.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void patch(String url, JSONObject params) {
        try {
            RequestQueue que = Volley.newRequestQueue(context);
            Request request = new JsonObjectRequest(Request.Method.PATCH, url, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    respCallback.onSuccessResponse(response, requestType);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        if(error.networkResponse.statusCode != 500) {
                            JSONObject errData = new JSONObject(Arrays.toString(error.networkResponse.data));
                            respCallback.onErrorResponse(errData);
                        } else {
                            JSONObject errData = new JSONObject();
                            errData.put("message", "Something went wrong with server");
                            respCallback.onErrorResponse(errData);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            que.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
