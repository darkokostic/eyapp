package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.NewsListSingleViewTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.NewsItem;

/**
 * Created by darko on 31.7.18..
 */

public class NewsListTemplateAdapter extends RecyclerView.Adapter<NewsListTemplateAdapter.NewsListTemplateViewHolder> {

    private ArrayList<NewsItem> data;
    private LayoutInflater inflater;
    private FragmentActivity activity;
    private Context context;
    private String pageTitle;

    public NewsListTemplateAdapter(String pageTitle, Context context, FragmentActivity activity, ArrayList<NewsItem> data) {
        this.data = data;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.pageTitle = pageTitle;
    }

    @Override
    public NewsListTemplateAdapter.NewsListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_news_item_template, parent, false);
        return new NewsListTemplateAdapter.NewsListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsListTemplateAdapter.NewsListTemplateViewHolder holder, final int position) {
        final NewsItem newsItem = data.get(position);

        Drawable defaultIcon = context.getResources().getDrawable(R.drawable.news_default);
        String thumbnailUrl = newsItem.getThumbnailUrl();

        holder.txtTitle.setText(newsItem.getTitle());
        holder.txtDate.setText(newsItem.getDate());
        holder.txtTime.setText(newsItem.getTime());

        if (thumbnailUrl == null) {
            holder.imgThumbnail.setImageDrawable(defaultIcon);
        } else {
            Picasso.with(context)
                    .load(thumbnailUrl)
                    .error(defaultIcon)
                    .into(holder.imgThumbnail);
        }

        holder.newsItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, NewsListSingleViewTemplate.class);
                intent.putExtra("id", newsItem.getId());
                intent.putExtra("title", pageTitle);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class NewsListTemplateViewHolder extends RecyclerView.ViewHolder {
        LinearLayout newsItemLayout;
        ImageView imgThumbnail;
        TextView txtTitle;
        TextView txtDate;
        TextView txtTime;

        public NewsListTemplateViewHolder(View itemView) {
            super(itemView);
            newsItemLayout = itemView.findViewById(R.id.news_item_layout);
            imgThumbnail = itemView.findViewById(R.id.img_news_thumbnail);
            txtTitle = itemView.findViewById(R.id.txt_news_title);
            txtDate = itemView.findViewById(R.id.txt_news_date);
            txtTime = itemView.findViewById(R.id.txt_news_time);
        }
    }
}
