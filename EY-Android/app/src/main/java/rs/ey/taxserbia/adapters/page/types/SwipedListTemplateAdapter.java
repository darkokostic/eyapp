package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.SwipedListSingleViewTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.TaxNewsItem;

/**
 * Created by darko on 31.7.18..
 */

public class SwipedListTemplateAdapter extends RecyclerView.Adapter<SwipedListTemplateAdapter.SwipedListTemplateViewHolder> {

    private ArrayList<TaxNewsItem> data;
    private LayoutInflater inflater;
    private FragmentActivity activity;
    private Context context;
    private String pageTitle;

    public SwipedListTemplateAdapter(String pageTitle, Context context, FragmentActivity activity, ArrayList<TaxNewsItem> data) {
        this.data = data;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.pageTitle = pageTitle;
    }

    @Override
    public SwipedListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_swiped_list_item_template, parent, false);
        return new SwipedListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SwipedListTemplateViewHolder holder, final int position) {
        final TaxNewsItem taxNewsItem = data.get(position);

        Drawable defaultIcon = context.getResources().getDrawable(R.drawable.news_default);
        String thumbnailUrl = taxNewsItem.getThumbnailUrl();

        holder.txtTitle.setText(taxNewsItem.getTitle());
        holder.txtDate.setText(taxNewsItem.getDate());
        holder.txtTime.setText(taxNewsItem.getTime());

        if (thumbnailUrl == null) {
            holder.imgThumbnail.setImageDrawable(defaultIcon);
        } else {
            Picasso.with(context)
                    .load(thumbnailUrl)
                    .error(defaultIcon)
                    .into(holder.imgThumbnail);
        }

        holder.swipedItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SwipedListSingleViewTemplate.class);
                intent.putExtra("id", taxNewsItem.getId());
                intent.putExtra("title", pageTitle);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class SwipedListTemplateViewHolder extends RecyclerView.ViewHolder {
        LinearLayout swipedItemLayout;
        ImageView imgThumbnail;
        TextView txtTitle;
        TextView txtDate;
        TextView txtTime;

        public SwipedListTemplateViewHolder(View itemView) {
            super(itemView);
            swipedItemLayout = itemView.findViewById(R.id.swiped_item_layout);
            imgThumbnail = itemView.findViewById(R.id.img_swiped_item_thumbnail);
            txtTitle = itemView.findViewById(R.id.txt_swiped_item_title);
            txtDate = itemView.findViewById(R.id.txt_swiped_item_date);
            txtTime = itemView.findViewById(R.id.txt_swiped_item_time);
        }
    }
}
