package rs.ey.taxserbia.activities.intro.steps;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ScrollView;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.intro.IntroActivity;

public class SecondIntroFragment extends Fragment {

    public SecondIntroFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_second_intro, container, false);

        WebView wvContent = view.findViewById(R.id.web_view_content);
        wvContent.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
                return true;
            }
        });
        String htmlContent = "<style>iframe{display: inline;height: auto;max-width: 100%;} img{display: inline;height: auto;max-width: 100%;} p{color: #646464;}</style>" + IntroActivity.txtIntroTerms;
        wvContent.loadData(htmlContent, "text/html", "utf-8");

        ScrollView scrollView = view.findViewById(R.id.intro_scroll);
        wvContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                IntroActivity.gestureDetectorCompat.onTouchEvent(event);
                return false;
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                IntroActivity.gestureDetectorCompat.onTouchEvent(event);
                return false;
            }
        });

        return view;
    }
}
