package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 19.9.18..
 */

public class PollAnswerItem {

    private String id;
    private String votes;
    private String option;
    private String answerCountChar;

    public PollAnswerItem(String id, String votes, String option, String answerCountChar) {
        this.id = id;
        this.votes = votes;
        this.option = option;
        this.answerCountChar = answerCountChar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getAnswerCountChar() {
        return answerCountChar;
    }

    public void setAnswerCountChar(String answerCountChar) {
        this.answerCountChar = answerCountChar;
    }
}
