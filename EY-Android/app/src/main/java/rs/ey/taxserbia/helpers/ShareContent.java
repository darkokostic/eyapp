package rs.ey.taxserbia.helpers;

import android.content.Context;
import android.content.Intent;
import android.text.Html;

/**
 * Created by darko on 2.8.18..
 */

public class ShareContent {
    public static void share(Context context, String content) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            content = Html.fromHtml(content, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            content = Html.fromHtml(content).toString();
        }

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, content);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, ""));
    }

    public static void share(Context context, StringBuilder content) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, content.toString());
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, ""));
    }
}
