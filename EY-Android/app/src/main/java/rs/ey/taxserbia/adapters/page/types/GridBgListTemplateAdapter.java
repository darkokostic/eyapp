package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.GridIconTemplate;
import rs.ey.taxserbia.activities.app.pages.types.IndustryOverviewGridTemplate;
import rs.ey.taxserbia.activities.app.pages.types.NewsListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.SwipedListTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.GridBgPageTemplate;

import static rs.ey.taxserbia.entities.pages.types.PagesType.GRID_ICON;
import static rs.ey.taxserbia.entities.pages.types.PagesType.INDUSTRY_OVERVIEW_GRID;
import static rs.ey.taxserbia.entities.pages.types.PagesType.NEWS_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.SWIPED_LIST;

/**
 * Created by darko on 28.6.18..
 */

public class GridBgListTemplateAdapter extends RecyclerView.Adapter<GridBgListTemplateAdapter.GridBgListTemplateViewHolder> {

    private ArrayList<GridBgPageTemplate> data;
    private LayoutInflater inflater;
    private FragmentActivity activity;

    public GridBgListTemplateAdapter(Context context, FragmentActivity activity, ArrayList<GridBgPageTemplate> data) {
        this.data = data;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public GridBgListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_grid_bg_template, parent, false);

        return new GridBgListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridBgListTemplateViewHolder holder, final int position) {
        final GridBgPageTemplate gridBgPageItem = data.get(position);
        holder.txtGridBgItemTitle.setText(gridBgPageItem.getTitle());

        Picasso.with(activity)
                .load(gridBgPageItem.getImageUrl())
                .error(activity.getResources().getDrawable(R.drawable.aktuelnosti_cover))
                .into(holder.imgCover);

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (gridBgPageItem.getType()) {
                    case GRID_ICON:
                        Intent intent = new Intent(activity, GridIconTemplate.class);
                        intent.putExtra("id", gridBgPageItem.getId());
                        intent.putExtra("title", gridBgPageItem.getTitle());
                        activity.startActivity(intent);
                        break;
                    case NEWS_LIST:
                        intent = new Intent(activity, NewsListTemplate.class);
                        intent.putExtra("id", gridBgPageItem.getId());
                        intent.putExtra("title", gridBgPageItem.getTitle());
                        activity.startActivity(intent);
                        break;
                    case INDUSTRY_OVERVIEW_GRID:
                        intent = new Intent(activity, IndustryOverviewGridTemplate.class);
                        intent.putExtra("title", gridBgPageItem.getTitle());
                        activity.startActivity(intent);
                        break;
                    case SWIPED_LIST:
                        intent = new Intent(activity, SwipedListTemplate.class);
                        intent.putExtra("title", gridBgPageItem.getTitle());
                        activity.startActivity(intent);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class GridBgListTemplateViewHolder extends RecyclerView.ViewHolder {
        FrameLayout itemLayout;
        TextView txtGridBgItemTitle;
        ImageView imgCover;

        public GridBgListTemplateViewHolder(View itemView) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.grid_bg_item_layout);
            txtGridBgItemTitle = itemView.findViewById(R.id.txt_grid_bg_item_title);
            imgCover = itemView.findViewById(R.id.img_cover);
        }
    }
}
