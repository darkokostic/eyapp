package rs.ey.taxserbia.adapters.page.types;

import rs.ey.taxserbia.entities.pages.types.templates.PollItem;

/**
 * Created by darko on 19.9.18..
 */

public interface PollsListItemClickHandler {

    void openPollSingleView(PollItem item);
}
