package rs.ey.taxserbia.activities.intro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.jaeger.library.StatusBarUtil;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.helpers.LanguageHelper;

public class LanguageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        StatusBarUtil.setColor(LanguageActivity.this, getResources().getColor(R.color.status_bar_language));

        LinearLayout btnEnglish = (LinearLayout) findViewById(R.id.btnEnglish);
        LinearLayout btnSerbian = (LinearLayout) findViewById(R.id.btnSerbian);

        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EyApplication.chosenLanguage = getResources().getString(R.string.english_lg);
                EyApplication.langShort = "en";
                LanguageHelper.setLanguage(EyApplication.langShort, getApplicationContext());
                startActivity(new Intent(getApplicationContext(), IntroActivity.class));
                finish();
            }
        });

        btnSerbian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EyApplication.chosenLanguage = getResources().getString(R.string.serbian_lg);
                EyApplication.langShort = "sr";
                LanguageHelper.setLanguage(EyApplication.langShort, getApplicationContext());
                startActivity(new Intent(getApplicationContext(), IntroActivity.class));
                finish();
            }
        });
    }
}
