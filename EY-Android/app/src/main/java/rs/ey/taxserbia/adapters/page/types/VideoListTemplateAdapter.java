package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.NewsListSingleViewTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.VideoItem;

/**
 * Created by darko on 3.9.18..
 */

public class VideoListTemplateAdapter extends RecyclerView.Adapter<VideoListTemplateAdapter.VideoListTemplateViewHolder> {

    private ArrayList<VideoItem> data;
    private LayoutInflater inflater;
    private FragmentActivity activity;
    private Context context;
    private String pageTitle;

    public VideoListTemplateAdapter(String pageTitle, Context context, FragmentActivity activity, ArrayList<VideoItem> data) {
        this.data = data;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.pageTitle = pageTitle;
    }

    @Override
    public VideoListTemplateAdapter.VideoListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_video_item_template, parent, false);
        return new VideoListTemplateAdapter.VideoListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoListTemplateAdapter.VideoListTemplateViewHolder holder, final int position) {
        final VideoItem videoItem = data.get(position);

        Drawable defaultIcon = context.getResources().getDrawable(R.drawable.news_default);
        String thumbnailUrl = videoItem.getThumbnailUrl();

        holder.txtTitle.setText(videoItem.getTitle());
        holder.txtDate.setText(videoItem.getDate());
        holder.txtTime.setText(videoItem.getTime());

        if (thumbnailUrl == null) {
            holder.imgThumbnail.setImageDrawable(defaultIcon);
        } else {
            Picasso.with(context)
                    .load(thumbnailUrl)
                    .error(defaultIcon)
                    .into(holder.imgThumbnail);
        }

        holder.videoItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, NewsListSingleViewTemplate.class);
                intent.putExtra("id", videoItem.getId());
                intent.putExtra("title", pageTitle);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class VideoListTemplateViewHolder extends RecyclerView.ViewHolder {
        LinearLayout videoItemLayout;
        ImageView imgThumbnail;
        TextView txtTitle;
        TextView txtDate;
        TextView txtTime;

        public VideoListTemplateViewHolder(View itemView) {
            super(itemView);
            videoItemLayout = itemView.findViewById(R.id.news_item_layout);
            imgThumbnail = itemView.findViewById(R.id.img_news_thumbnail);
            txtTitle = itemView.findViewById(R.id.txt_news_title);
            txtDate = itemView.findViewById(R.id.txt_news_date);
            txtTime = itemView.findViewById(R.id.txt_news_time);
        }
    }
}
