package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarObligationSingleViewTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.CalendarObligation;

/**
 * Created by darko on 31.7.18..
 */

public class CalendarObligationsAdapter extends RecyclerView.Adapter<CalendarObligationsAdapter.CalendarObligationsViewHolder> {

    private ArrayList<CalendarObligation> data;
    private LayoutInflater inflater;
    private Context context;
    private String title;

    public CalendarObligationsAdapter(String title, Context context, ArrayList<CalendarObligation> data) {
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.title = title;
    }

    @Override
    public CalendarObligationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_obligations_calendar_item, parent, false);
        return new CalendarObligationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CalendarObligationsViewHolder holder, final int position) {
        final CalendarObligation obligation = data.get(position);

        holder.txtDate.setText(obligation.getDate());
        holder.txtName.setText(obligation.getName());
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CalendarObligationSingleViewTemplate.class);
                intent.putExtra("title", title);
                intent.putExtra("id", obligation.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CalendarObligationsViewHolder extends RecyclerView.ViewHolder {

        LinearLayout itemLayout;
        TextView txtDate;
        TextView txtName;

        public CalendarObligationsViewHolder(View itemView) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.obligations_item_layout);
            txtDate = itemView.findViewById(R.id.txt_date);
            txtName = itemView.findViewById(R.id.txt_name);
        }
    }
}
