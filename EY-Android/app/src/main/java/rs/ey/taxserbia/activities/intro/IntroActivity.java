package rs.ey.taxserbia.activities.intro;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.intro.steps.FirstIntroFragment;
import rs.ey.taxserbia.activities.intro.steps.FourthIntroFragment;
import rs.ey.taxserbia.activities.intro.steps.IntroCurrentSettingsFragment;
import rs.ey.taxserbia.activities.intro.steps.SecondIntroFragment;
import rs.ey.taxserbia.activities.intro.steps.ThirdIntroFragment;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.LocalStorage;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

@SuppressWarnings("AccessStaticViaInstance")
public class IntroActivity extends AppCompatActivity implements Responsable {

    private static int stepId;
    private static final int FIRST_STEP = 1;
    private static final int SECOND_STEP = 2;
    private static final int THIRD_STEP = 3;
    private static final int FOURTH_STEP = 4;
    private static final int FIFTH_STEP = 5;

    private LinearLayout tabsLayout;
    private LinearLayout btnContinue;
    private TextView txtContinue;

    // Tabs components
    private FrameLayout tab1Layout;
    private FrameLayout tab2Layout;
    private FrameLayout tab3Layout;
    private FrameLayout tab4Layout;
    private ImageView imgTab1;
    private ImageView imgTab2;
    private ImageView imgTab3;
    private ImageView imgTab4;
    private View tab1BorderActive;
    private View tab2BorderActive;
    private View tab3BorderActive;
    private View tab4BorderActive;
    private View tab1BorderBottom;
    private View tab2BorderBottom;
    private View tab3BorderBottom;
    private View tab4BorderBottom;
    private FrameLayout contentContainer;

    private VolleyService httpService;
    private final static String HTTP_URL = "https://ey.nbgcreator.com/api/config";

    private LinearLayout loadingLayout;
    private ProgressBar progressBar;

    public static GestureDetectorCompat gestureDetectorCompat;

    public static String txtIntroAboutApp;
    public static String txtIntroTerms;
    public static String txtIntroPush;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.setLanguage(EyApplication.langShort, getApplicationContext());
        setContentView(R.layout.activity_intro);

        StatusBarUtil.setColor(IntroActivity.this, getResources().getColor(R.color.status_bar_intro));

        initComponents();
        initIndustriesValues();
        if (CheckInternetConnection.isOnline(this)) {
            getIntroData();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }

        // init sharing object values
        stepId = FIRST_STEP;

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stepId < FIFTH_STEP) {
                    nextStep();
                } else {
                    LocalStorage.setLanguageLong(getApplicationContext(), EyApplication.chosenLanguage);
                    LocalStorage.setLanguageShort(getApplicationContext(), EyApplication.langShort);
                    LocalStorage.setChosenIndustries(getApplicationContext(), EyApplication.chosenIndustries);
                    LocalStorage.setHasIntro(getApplicationContext(), false);
                    startActivity(new Intent(IntroActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });

        gestureDetectorCompat = new GestureDetectorCompat(getApplicationContext(), new MyGestureListener());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initIndustriesValues() {
        EyApplication.chosenIndustries = new ArrayList<>();
        EyApplication.cbAllStatus = false;
        EyApplication.cbPushStatus = true;
        LocalStorage.setPushNotificationsStatus(this, true);
        OneSignal.setSubscription(EyApplication.cbPushStatus);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private void swipeLeft() {
        if (stepId < FIFTH_STEP) {
            nextStep();
        }
    }

    private void swipeRight() {
        if (stepId > FIRST_STEP) {
            stepId--;
            this.changeStep();
        }
    }

    /**
     *  nextStep: This method increment stepId value and call changeStep method to change page
     * */
    private void nextStep() {
        if (stepId == FOURTH_STEP && EyApplication.chosenIndustries.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.intro_fourth_msg), Toast.LENGTH_LONG).show();
        } else {
            stepId++;
            this.changeStep();
        }
    }

    private void changeStep() {
        switch (stepId) {
            case FIRST_STEP:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, new FirstIntroFragment())
                        .commit();

                setTabsIcons(getResources().getDrawable(R.drawable.tab1_icon_active),
                        getResources().getDrawable(R.drawable.tab2_icon_inactive),
                        getResources().getDrawable(R.drawable.tab3_icon_inactive),
                        getResources().getDrawable(R.drawable.tab4_icon_inactive));

                tab1Layout.setBackgroundColor(getResources().getColor(R.color.intro_active_tab_bg));
                tab2Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab3Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab4Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));

                tab1Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.4f));
                tab2Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab3Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab4Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));

                tab1BorderActive.setVisibility(View.VISIBLE);
                tab2BorderActive.setVisibility(View.GONE);
                tab3BorderActive.setVisibility(View.GONE);
                tab4BorderActive.setVisibility(View.GONE);

                tab1BorderBottom.setVisibility(View.GONE);
                tab2BorderBottom.setVisibility(View.VISIBLE);
                tab3BorderBottom.setVisibility(View.VISIBLE);
                tab4BorderBottom.setVisibility(View.VISIBLE);

                btnContinue.setBackgroundColor(getResources().getColor(R.color.intro_continue_btn_bg));
                txtContinue.setTextColor(getResources().getColor(R.color.intro_continue_btn_color));
                break;
            case SECOND_STEP:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, new SecondIntroFragment())
                        .commit();
                setTabsIcons(getResources().getDrawable(R.drawable.tab1_icon_inactive),
                        getResources().getDrawable(R.drawable.tab2_icon_active),
                        getResources().getDrawable(R.drawable.tab3_icon_inactive),
                        getResources().getDrawable(R.drawable.tab4_icon_inactive));

                tab1Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab2Layout.setBackgroundColor(getResources().getColor(R.color.intro_active_tab_bg));
                tab3Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab4Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));

                tab1Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab2Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.4f));
                tab3Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab4Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));

                tab1BorderActive.setVisibility(View.GONE);
                tab2BorderActive.setVisibility(View.VISIBLE);
                tab3BorderActive.setVisibility(View.GONE);
                tab4BorderActive.setVisibility(View.GONE);

                tab1BorderBottom.setVisibility(View.VISIBLE);
                tab2BorderBottom.setVisibility(View.GONE);
                tab3BorderBottom.setVisibility(View.VISIBLE);
                tab4BorderBottom.setVisibility(View.VISIBLE);

                btnContinue.setBackgroundColor(getResources().getColor(R.color.intro_continue_btn_bg));
                txtContinue.setTextColor(getResources().getColor(R.color.intro_continue_btn_color));
                break;
            case THIRD_STEP:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, new ThirdIntroFragment())
                        .commit();
                setTabsIcons(getResources().getDrawable(R.drawable.tab1_icon_inactive),
                        getResources().getDrawable(R.drawable.tab2_icon_inactive),
                        getResources().getDrawable(R.drawable.tab3_icon_active),
                        getResources().getDrawable(R.drawable.tab4_icon_inactive));

                tab1Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab2Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab3Layout.setBackgroundColor(getResources().getColor(R.color.intro_active_tab_bg));
                tab4Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));

                tab1Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab2Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab3Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.4f));
                tab4Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));

                tab1BorderActive.setVisibility(View.GONE);
                tab2BorderActive.setVisibility(View.GONE);
                tab3BorderActive.setVisibility(View.VISIBLE);
                tab4BorderActive.setVisibility(View.GONE);

                tab1BorderBottom.setVisibility(View.VISIBLE);
                tab2BorderBottom.setVisibility(View.VISIBLE);
                tab3BorderBottom.setVisibility(View.GONE);
                tab4BorderBottom.setVisibility(View.VISIBLE);

                btnContinue.setBackgroundColor(getResources().getColor(R.color.intro_continue_btn_bg));
                txtContinue.setTextColor(getResources().getColor(R.color.intro_continue_btn_color));
                break;
            case FOURTH_STEP:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, new FourthIntroFragment())
                        .commit();
                setTabsIcons(getResources().getDrawable(R.drawable.tab1_icon_inactive),
                        getResources().getDrawable(R.drawable.tab2_icon_inactive),
                        getResources().getDrawable(R.drawable.tab3_icon_inactive),
                        getResources().getDrawable(R.drawable.tab4_icon_active));

                tab1Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab2Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab3Layout.setBackgroundColor(getResources().getColor(R.color.intro_inactive_tab_bg));
                tab4Layout.setBackgroundColor(getResources().getColor(R.color.intro_active_tab_bg));

                tab1Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab2Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab3Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.2f));
                tab4Layout.setLayoutParams(new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        0.4f));

                tab1BorderActive.setVisibility(View.GONE);
                tab2BorderActive.setVisibility(View.GONE);
                tab3BorderActive.setVisibility(View.GONE);
                tab4BorderActive.setVisibility(View.VISIBLE);

                tab1BorderBottom.setVisibility(View.VISIBLE);
                tab2BorderBottom.setVisibility(View.VISIBLE);
                tab3BorderBottom.setVisibility(View.VISIBLE);
                tab4BorderBottom.setVisibility(View.GONE);

                txtContinue.setText(getResources().getString(R.string.intro_continue));
                tabsLayout.setVisibility(View.VISIBLE);
                contentContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        0,
                        0.8f));

                btnContinue.setBackgroundColor(getResources().getColor(R.color.intro_continue_btn_bg));
                txtContinue.setTextColor(getResources().getColor(R.color.intro_continue_btn_color));
                break;
            case FIFTH_STEP:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, new IntroCurrentSettingsFragment())
                        .commit();
                txtContinue.setText(getResources().getString(R.string.intro_run_app));
                btnContinue.setBackgroundColor(getResources().getColor(R.color.intro_run_app_btn_bg));
                txtContinue.setTextColor(getResources().getColor(R.color.intro_run_app_btn_color));
                tabsLayout.setVisibility(View.GONE);
                contentContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        0,
                        0.95f));
                break;
        }
    }

    private void setTabsIcons(Drawable tab1Icon, Drawable tab2Icon, Drawable tab3Icon, Drawable tab4Icon) {
        imgTab1.setImageDrawable(tab1Icon);
        imgTab2.setImageDrawable(tab2Icon);
        imgTab3.setImageDrawable(tab3Icon);
        imgTab4.setImageDrawable(tab4Icon);
    }

    private void initComponents() {
        // init view elements
        tabsLayout = findViewById(R.id.tabsLayout);
        btnContinue = findViewById(R.id.btnContinue);
        txtContinue = findViewById(R.id.txtContinue);
        contentContainer = findViewById(R.id.contentContainer);

        // Tab1
        tab1Layout = findViewById(R.id.tab1Layout);
        imgTab1 = findViewById(R.id.imgTab1);
        tab1BorderActive = findViewById(R.id.tab1_border_top_active);
        tab1BorderBottom = findViewById(R.id.tab1_border_bottom);

        // Tab2
        tab2Layout = findViewById(R.id.tab2Layout);
        imgTab2 = findViewById(R.id.imgTab2);
        tab2BorderActive = findViewById(R.id.tab2_border_top_active);
        tab2BorderBottom = findViewById(R.id.tab2_border_bottom);

        // Tab3
        tab3Layout = findViewById(R.id.tab3Layout);
        imgTab3 = findViewById(R.id.imgTab3);
        tab3BorderActive = findViewById(R.id.tab3_border_top_active);
        tab3BorderBottom = findViewById(R.id.tab3_border_bottom);

        // Tab4
        tab4Layout = findViewById(R.id.tab4Layout);
        imgTab4 = findViewById(R.id.imgTab4);
        tab4BorderActive = findViewById(R.id.tab4_border_top_active);
        tab4BorderBottom = findViewById(R.id.tab4_border_bottom);

        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        loadingLayout.setVisibility(View.VISIBLE);
        contentContainer.setVisibility(View.INVISIBLE);
        httpService = new VolleyService(this, this);
    }

    private void getIntroData() {
        String params = "?keys[]=mob_intro_app_en&keys[]=mob_intro_push_en&keys[]=mob_intro_terms_en";
        if (EyApplication.langShort.equals("sr")) {
            params = "?keys[]=mob_intro_app_sr&keys[]=mob_intro_push_sr&keys[]=mob_intro_terms_sr";
        }

        httpService.get(HTTP_URL + params);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            if (EyApplication.langShort.equals("sr")) {
                txtIntroAboutApp = entity.getString("mob_intro_app_sr");
                txtIntroTerms = entity.getString("mob_intro_terms_sr");
                txtIntroPush = entity.getString("mob_intro_push_sr");
            } else {
                txtIntroAboutApp = entity.getString("mob_intro_app_en");
                txtIntroTerms = entity.getString("mob_intro_terms_en");
                txtIntroPush = entity.getString("mob_intro_push_en");
            }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentContainer, new FirstIntroFragment())
                    .commit();

            loadingLayout.setVisibility(View.GONE);
            contentContainer.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(this, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            try {
                if (event2.getX() < event1.getX()) {
                    swipeLeft();
                }

                if (event2.getX() > event1.getX()) {
                    swipeRight();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            return true;
        }
    }
}
