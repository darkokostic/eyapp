package rs.ey.taxserbia.adapters.page.types;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import rs.ey.taxserbia.R;

/**
 * Created by darko on 9.7.18..
 */

@SuppressWarnings({"ConstantConditions", "DefaultFileTemplate"})
public class ExpandableListTemplateAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listParentQuestions;
    private HashMap<String, List<String>> listChildAnswers;

    public ExpandableListTemplateAdapter(Context context, List<String> listParentQuestions,
                                         HashMap<String, List<String>> listChildAnswers) {
        this.context = context;
        this.listParentQuestions = listParentQuestions;
        this.listChildAnswers = listChildAnswers;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listChildAnswers.get(this.listParentQuestions.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String answerData = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.faq_exp_list_subitem, null);
        }

        TextView txtAnswer = convertView.findViewById(R.id.txtFaqAnswer);
        txtAnswer.setText(Html.fromHtml(answerData));

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listChildAnswers.get(this.listParentQuestions.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listParentQuestions.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listParentQuestions.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerQuestion = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.faq_exp_list_item, null);
        }

        TextView txtQuestion = convertView.findViewById(R.id.txtFaqQuestion);
        ImageView indicatorImg = convertView.findViewById(R.id.imgIndicator);

        txtQuestion.setText(headerQuestion);
        if (isExpanded) {
            indicatorImg.setImageResource(R.drawable.ic_faq_item_minus);
        } else {
            indicatorImg.setImageResource(R.drawable.ic_faq_item_plus);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
