package rs.ey.taxserbia.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.VideoView;

import com.jaeger.library.StatusBarUtil;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.intro.LanguageActivity;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.services.LocalStorage;

public class SplashActivity extends AppCompatActivity {

    private static final int splashInterval = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        StatusBarUtil.setColor(SplashActivity.this, getResources().getColor(R.color.status_bar_splash));

        VideoView view = findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.splash;
        view.setVideoURI(Uri.parse(path));
        view.start();

        EyApplication.langShort = LocalStorage.getLanguageShort(this);
        EyApplication.chosenLanguage = LocalStorage.getLanguageLong(this);
        EyApplication.chosenIndustries = LocalStorage.getChosenIndustries(this);
        EyApplication.cbAllStatus = LocalStorage.getIsSelectedAll(this);

        LanguageHelper.setLanguage(EyApplication.langShort, getApplicationContext());

        new CountDownTimer(splashInterval, splashInterval) {
            @Override
            public void onTick(long millisUntilFinished) {}

            @Override
            public void onFinish() {
                if (LocalStorage.getHasIntro(getApplicationContext())) {
                    startActivity(new Intent(SplashActivity.this, LanguageActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                }
                finish();
            }
        }.start();
    }
}
