package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.PollResultsTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.PollItem;

/**
 * Created by darko on 28.6.18..
 */

public class PollsArchiveListTemplateAdapter extends RecyclerView.Adapter<PollsArchiveListTemplateAdapter.PollsArchiveListTemplateViewHolder> {

    private ArrayList<PollItem> data;
    private LayoutInflater inflater;
    private Context context;
    private PollsListItemClickHandler pollClickHandler;

    public PollsArchiveListTemplateAdapter(Context context, ArrayList<PollItem> data,
                                           PollsListItemClickHandler pollClickHandler) {
        this.data = data;
        this.context = context;
        this.pollClickHandler = pollClickHandler;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public PollsArchiveListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_polls_archive_list_template, parent, false);
        return new PollsArchiveListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PollsArchiveListTemplateViewHolder holder, final int position) {
        final PollItem pollItem = data.get(position);

        holder.txtDate.setText(pollItem.getDate());
        holder.txtQuestion.setText(pollItem.getName());
        holder.layoutPollItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PollResultsTemplate.class);
                intent.putExtra("title", "");
                intent.putExtra("id", pollItem.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PollsArchiveListTemplateViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutPollItem;
        TextView txtDate;
        TextView txtQuestion;

        public PollsArchiveListTemplateViewHolder(View itemView) {
            super(itemView);
            layoutPollItem = itemView.findViewById(R.id.layout_poll_item);
            txtDate = itemView.findViewById(R.id.txt_date);
            txtQuestion = itemView.findViewById(R.id.txt_question);
        }
    }
}
