package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 17.7.18..
 */

public class GridIconItemTemplate {
    private String id;
    private String objectId;
    private String title;
    private String iconUrl;
    private String type;

    public GridIconItemTemplate(String id, String objectId, String title, String iconUrl, String type) {
        this.id = id;
        this.objectId = objectId;
        this.title = title;
        this.iconUrl = iconUrl;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
