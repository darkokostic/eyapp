package rs.ey.taxserbia.activities.app.pages.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.adapters.page.types.NewsListTemplateAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.entities.pages.types.templates.NewsItem;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.helpers.ShareContent;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

@SuppressWarnings("ConstantConditions")
public class NewsListSingleViewTemplate extends AppCompatActivity implements Responsable {

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private VolleyService httpService;
    private final static String NEWS_SINGLE_HTTP_URL = "https://ey.nbgcreator.com/api/news/article";
    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private final static String READ_FLAG_HTTP_URL = "https://ey.nbgcreator.com/api/news/read";

    private final static int REQUEST_TYPE_SEARCH = 2;
    private final static int REQUEST_TYPE_BASIC = 1;
    private final static int REQUEST_TYPE_READ = 3;

    private RecyclerView relatedNewsRecyclerView;
    private TextView txtTitle;
    private ArrayList<NewsItem> relatedNewsList;
    private TextView txtArticleTitle;
    private TextView txtArticleDate;
    private TextView txtArticleTime;
    private WebView wvContent;
    private ImageView coverImage;
    private ImageView btnShare;
    private Context context;
    private String shareableContent;

    private LinearLayout contentView;
    private LinearLayout loadingLayout;
    private ProgressBar progressBar;
    private ImageView btnRelatedPrevious;
    private ImageView btnRelatedNext;
    private NestedScrollView scrollView;
    private LinearLayout layoutRelatedNews;

    private String id;
    private String title;
    private int shortRelatedNewsIndex = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_news_list_single_view);

        context = this;

        id = getIntent().getExtras().getString("id");
        title = getIntent().getExtras().getString("title");

        relatedNewsRecyclerView = findViewById(R.id.related_news_recycler_view);

        StatusBarUtil.setColor(NewsListSingleViewTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (!title.equals("Notification")) {
            txtTitle.setText(title);
        }

        if (CheckInternetConnection.isOnline(this)) {
            requestNewsSingleViewData();
            requestRead();
        } else {
            loadingLayout.setVisibility(View.GONE);
            Toast.makeText(this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        contentView = findViewById(R.id.content_view);
        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);

        contentView.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);

        btnRelatedPrevious = findViewById(R.id.btnRelatedPrevious);
        btnRelatedNext = findViewById(R.id.btnRelatedNext);
        btnRelatedPrevious.setVisibility(View.GONE);
        btnRelatedNext.setVisibility(View.VISIBLE);
        scrollView = findViewById(R.id.scrollView);

        txtTitle = findViewById(R.id.txt_title);
        txtArticleTitle = findViewById(R.id.txt_article_title);
        txtArticleDate = findViewById(R.id.txt_article_date);
        txtArticleTime = findViewById(R.id.txt_article_time);
        wvContent = findViewById(R.id.web_view_content);
        coverImage = findViewById(R.id.img_cover);
        btnShare = findViewById(R.id.btn_share);
        httpService = new VolleyService(this, this, REQUEST_TYPE_BASIC);
        layoutRelatedNews = findViewById(R.id.layout_related_news);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsListSingleViewTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!title.equals("Notification")) {
                    finish();
                } else {
                    startActivity(new Intent(NewsListSingleViewTemplate.this, HomeActivity.class));
                    finish();
                }
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(NewsListSingleViewTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsListSingleViewTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsListSingleViewTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsListSingleViewTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = txtArticleTitle.getText().toString()
                        + "\n"
                        + "\n"
                        + shareableContent;
                ShareContent.share(context, content);
            }
        });

        btnRelatedPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPreviousRelatedNews();
            }
        });

        btnRelatedNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNextRelatedNews();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(NewsListSingleViewTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    private void createListLayout() {
        ArrayList<NewsItem> shortRelatedNews = new ArrayList<>();

        getNextRelatedNews(shortRelatedNews);

        relatedNewsRecyclerView.setHasFixedSize(true);
        final NewsListTemplateAdapter newsListAdapter = new NewsListTemplateAdapter(title,this, this, shortRelatedNews);
        relatedNewsRecyclerView.setAdapter(newsListAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        relatedNewsRecyclerView.setLayoutManager(layoutManager);
    }

    private void createNewsItem(ArrayList<NewsItem> newsItemsList, String id, String title, String date, String time, String coverImageUrl) {
        NewsItem newsItem = new NewsItem(id, title, date, time, coverImageUrl);
        newsItemsList.add(newsItem);
    }

    private void requestNewsSingleViewData() {
        relatedNewsList = new ArrayList<>();
        httpService = new VolleyService(this, this, REQUEST_TYPE_BASIC);
        contentView.setVisibility(View.GONE);
        String params = "?lang="
                + EyApplication.langShort
                + "&id=" + id;
        httpService.get(NEWS_SINGLE_HTTP_URL + params);
    }

    private void requestRead() {
        httpService = new VolleyService(this, this, REQUEST_TYPE_READ);
        JSONObject params = new JSONObject();
        try {
            params.put("id", id);
            httpService.patch(READ_FLAG_HTTP_URL, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getPreviousRelatedNews() {
        ArrayList<NewsItem> shortRelatedNews = new ArrayList<>();

        shortRelatedNewsIndex -= 2;

        getPreviousRelatedNews(shortRelatedNews);

        relatedNewsRecyclerView.setHasFixedSize(true);
        final NewsListTemplateAdapter newsListAdapter = new NewsListTemplateAdapter(title,this, this, shortRelatedNews);
        relatedNewsRecyclerView.setAdapter(newsListAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        relatedNewsRecyclerView.setLayoutManager(layoutManager);

        btnRelatedNext.setVisibility(View.VISIBLE);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    private void getNextRelatedNews() {
        ArrayList<NewsItem> shortRelatedNews = new ArrayList<>();

        shortRelatedNewsIndex += 2;

        getNextRelatedNews(shortRelatedNews);

        relatedNewsRecyclerView.setHasFixedSize(true);
        final NewsListTemplateAdapter newsListAdapter = new NewsListTemplateAdapter(title,this, this, shortRelatedNews);
        relatedNewsRecyclerView.setAdapter(newsListAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        relatedNewsRecyclerView.setLayoutManager(layoutManager);

        btnRelatedPrevious.setVisibility(View.VISIBLE);
    }

    private void getPreviousRelatedNews(ArrayList<NewsItem> shortRelatedNews) {
        if (shortRelatedNewsIndex == 2) {
            btnRelatedPrevious.setVisibility(View.GONE);
        }

        for (int i = shortRelatedNewsIndex - 2; i < shortRelatedNewsIndex; i++) {
            shortRelatedNews.add(relatedNewsList.get(i));
        }
    }

    private void getNextRelatedNews(ArrayList<NewsItem> shortRelatedNews) {
        if (relatedNewsList.size() > shortRelatedNewsIndex) {
            btnRelatedNext.setVisibility(View.VISIBLE);
        } else {
            btnRelatedNext.setVisibility(View.GONE);
        }

        if (relatedNewsList.size() >= shortRelatedNewsIndex) {
            for (int i = shortRelatedNewsIndex - 2; i < shortRelatedNewsIndex; i++) {
                shortRelatedNews.add(relatedNewsList.get(i));
            }
        } else {
            for (int i = shortRelatedNewsIndex - 2; i < relatedNewsList.size(); i++) {
                shortRelatedNews.add(relatedNewsList.get(i));
            }
        }
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            if (requestType == REQUEST_TYPE_BASIC) {
                JSONObject entity = res.getJSONObject("entity");

                if (entity.has("related_articles")) {
                    JSONArray relatedNews = entity.getJSONArray("related_articles");

                    if (relatedNews.length() == 0) {
                        layoutRelatedNews.setVisibility(View.GONE);
                    } else {
                        for (int i = 0; i < relatedNews.length(); i++) {
                            JSONObject newsItem = relatedNews.getJSONObject(i);
                            String dateTime = newsItem.getString("publish_date");
                            createNewsItem(relatedNewsList, newsItem.getString("id"), newsItem.getString("title"), dateTime.split(" ")[0], dateTime.split(" ")[1], newsItem.getString("thumbnail"));
                        }
                        createListLayout();
                    }
                } else {
                    layoutRelatedNews.setVisibility(View.GONE);
                }

                txtArticleTitle.setText(entity.getString("title"));
                String dateTime = entity.getString("publish_date");
                txtArticleDate.setText(dateTime.split(" ")[0]);
                txtArticleTime.setText(dateTime.split(" ")[1] + "h");
                shareableContent = entity.getString("text");

                wvContent.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(i);
                        return true;
                    }
                });

                WebSettings webSettings = wvContent.getSettings();
                webSettings.setJavaScriptEnabled(true);

                String htmlContent = "<style>iframe{display: inline;height: auto;max-width: 100%;} img{display: inline;height: auto;max-width: 100%;}</style>" + entity.getString("text");
                wvContent.loadData(htmlContent, "text/html", "utf-8");

                String imageUrl = entity.getString("image");
                Picasso.with(this)
                        .load(imageUrl)
                        .error(getResources().getDrawable(R.drawable.news_default))
                        .into(coverImage);
            } else if (requestType == REQUEST_TYPE_SEARCH) {
                JSONArray entity = res.getJSONArray("entity");

                for (int i = 0; i < entity.length(); i++) {
                    JSONObject searchItem = entity.getJSONObject(i);
                    createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
                }

                updateSearchListResultRecyclerView();
            }

            contentView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        } catch (JSONException e) {
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        loadingLayout.setVisibility(View.GONE);
        try {
            Toast.makeText(getApplicationContext(), err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
