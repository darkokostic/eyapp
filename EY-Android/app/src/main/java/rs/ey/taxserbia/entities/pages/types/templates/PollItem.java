package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 19.9.18..
 */

public class PollItem {

    private String id;
    private String date;
    private String name;

    public PollItem(String id, String date, String name) {
        this.id = id;
        this.date = date;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
