package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.entities.pages.types.templates.CalculationResultItem;

public class CalculationResultsAdapter extends RecyclerView.Adapter<CalculationResultsAdapter.CalculationResultViewHolder> {

    private Context context;
    private ArrayList<CalculationResultItem> data;
    private LayoutInflater inflater;

    public CalculationResultsAdapter(Context context, ArrayList<CalculationResultItem> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CalculationResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_calculation_result_item_light_grey, parent, false);
        return new CalculationResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CalculationResultViewHolder holder, final int position) {
        final CalculationResultItem calculationResultItem = data.get(position);

        holder.txtKey.setText(calculationResultItem.getKey());
        holder.txtValue.setText(calculationResultItem.getValue());
        if (position % 2 == 0) {
            holder.layoutResultItem.setBackgroundColor(context.getResources().getColor(R.color.calc_result_item_light));
        } else {
            holder.layoutResultItem.setBackgroundColor(context.getResources().getColor(R.color.calc_result_item_dark));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CalculationResultViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutResultItem;
        TextView txtKey;
        TextView txtValue;
        public CalculationResultViewHolder(View itemView) {
            super(itemView);
            layoutResultItem = itemView.findViewById(R.id.layout_result_item);
            txtKey =itemView.findViewById(R.id.txt_key);
            txtValue =itemView.findViewById(R.id.txt_value);
        }
    }
}
