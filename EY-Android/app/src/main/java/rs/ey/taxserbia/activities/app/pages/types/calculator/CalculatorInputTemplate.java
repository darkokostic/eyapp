package rs.ey.taxserbia.activities.app.pages.types.calculator;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.helpers.NumberTextWatcherForThousand;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.ACTIVITY_CONTRACT;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B1_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B2_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B_N_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.NO_ANSWER;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B1_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B2_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_B_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.YES_ANSWER;

public class CalculatorInputTemplate extends AppCompatActivity implements Responsable {

    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private final static String HTTP_CONFIG_URL = "https://ey.nbgcreator.com/api/config";

    private final static int REQUEST_TYPE_SEARCH = 0;
    private final static int REQUEST_TYPE_1 = 1;
    private final static int REQUEST_TYPE_2 = 2;
    private final static int REQUEST_TYPE_3 = 3;
    private final static int REQUEST_TYPE_4 = 4;
    private final static int REQUEST_TYPE_5 = 5;

    private VolleyService httpService;

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private TextView txtOption1;
    private TextView txtOption2;

    private String title;
    private String option1;
    private String option2;
    private String option3;
    private int securityAnswer;
    private String activityName;

    private LinearLayout loadingLayout;
    private ProgressBar progressBar;
    private EditText etInput;
    private EditText etExchangeRate;
    private LinearLayout btnCalculate;
    private String calculation;
    private String calculationValute;

    private double mobileInputValue;

    // Calculation variables
    double d16;
    double d18;
    double d20;
    double d17;
    double d19;
    double d21;

    double e10;
    double e11;
    double e12;
    double e13;
    double e14;
    double e15;
    double e17;
    double e19;
    double e21;
    double e23;
    double e25;

    int circularFormulaCounter = 0;

    private ScrollView scrollView;
    private LinearLayout layoutInfo;
    private ImageView btnInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_calculator_input);

        title = getIntent().getExtras().getString("title");
        option1 = getIntent().getExtras().getString("option1");
        option2 = getIntent().getExtras().getString("option2");
        option3 = getIntent().getExtras().getString("option3");
        activityName = getIntent().getExtras().getString("activityName");
        calculation = getIntent().getExtras().getString("calculation");
        calculationValute = getIntent().getExtras().getString("calculationValute");

        if (activityName.equals(ACTIVITY_CONTRACT)) {
            securityAnswer = getIntent().getExtras().getInt("securityAnswer");
        }

        StatusBarUtil.setColor(CalculatorInputTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        if (calculationValute.equals(CalculatorHelper.RSD_VALUE)) {
            etExchangeRate.setVisibility(View.GONE);
        }

        switch (calculation) {
            case B1_N_VALUE:
                etInput.setHint(getResources().getString(R.string.calc_input_input_hint3));
                break;
            case B2_N_VALUE:
                etInput.setHint(getResources().getString(R.string.calc_input_input_hint4));
                break;
            case N_B1_VALUE:
            case N_B2_VALUE:
            case N_B_VALUE:
                etInput.setHint(getResources().getString(R.string.calc_input_input_hint1));
                break;
            case B_N_VALUE:
                etInput.setHint(getResources().getString(R.string.calc_input_input_hint2));
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        loadingLayout = findViewById(R.id.loading_layout);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.yellow_progress_bar), PorterDuff.Mode.MULTIPLY);
        loadingLayout.setVisibility(View.GONE);

        txtOption1 = findViewById(R.id.txt_option_1);
        txtOption2 = findViewById(R.id.txt_option_2);

        txtOption1.setText(option1);
        String billingTypeText = getResources().getText(R.string.calc_billing_type_label) + " " + option2;
        txtOption2.setText(billingTypeText);
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        etInput = findViewById(R.id.et_input);
        etExchangeRate = findViewById(R.id.et_input2);
        btnCalculate = findViewById(R.id.btn_calculate);

        scrollView = findViewById(R.id.scroll_view);
        layoutInfo = findViewById(R.id.layout_info);
        btnInfo = findViewById(R.id.btn_info);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(CalculatorInputTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        if (CheckInternetConnection.isOnline(getApplicationContext())) {
                            requestSearch(query);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorInputTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileInputValue = Float.parseFloat(NumberTextWatcherForThousand.trimCommaOfString(etInput.getText().toString()));

                if (CheckInternetConnection.isOnline(getApplicationContext())) {
                    initValues();
                    switch (calculation) {
                        case B1_N_VALUE:
                            countB1N();
                            break;
                        case B2_N_VALUE:
                            countB2N();
                            break;
                        case N_B1_VALUE:
                        case N_B2_VALUE:
                            countNB1B2();
                            break;
                        case N_B_VALUE:
                            countNB();
                            break;
                        case B_N_VALUE:
                            countBN();
                            break;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0, layoutInfo.getTop());
            }
        });

        etInput.addTextChangedListener(new NumberTextWatcherForThousand(etInput));
    }

    private void initValues() {
        d16 = 0.0;
        d18 = 0.0;
        d20 = 0.0;
        d17 = 0.0;
        d19 = 0.0;
        d21 = 0.0;

        e10 = 0.0;
        e11 = 0.0;
        e12 = 0.0;
        e13 = 0.0;
        e14 = 0.0;
        e15 = 0.0;
        e17 = 0.0;
        e19 = 0.0;
        e21 = 0.0;
        e23 = 0.0;
        e25 = 0.0;

        circularFormulaCounter = 0;

        if (calculationValute.equals(CalculatorHelper.RSD_VALUE)) {
            e10 = mobileInputValue;
        } else {
            float exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
            e10 = mobileInputValue * exchangeRate;
        }
    }

    private void countBN() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_5);
        String params = "?keys[]=mob_calculator_standard_costs&keys[]=mob_calculator_personal_income_tax&keys[]=mob_calculator_contribution_to_pension_fund&keys[]=mob_calculator_contribution_to_health_fund";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    private void countNB() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_4);
        String params = "?keys[]=mob_calculator_standard_costs&keys[]=mob_calculator_personal_income_tax&keys[]=mob_calculator_contribution_to_pension_fund&keys[]=mob_calculator_contribution_to_health_fund";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    private void countNB1B2() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_3);
        String params = "?keys[]=mob_calculator_max_base_social_contributions&keys[]=mob_calculator_non_taxable_amount&keys[]=mob_calculator_salary_tax&keys[]=mob_calculator_social_contributions_employee&keys[]=mob_calculator_social_contributions_employer";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    private void countB2N() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_2);
        String params = "?keys[]=mob_calculator_max_base_social_contributions&keys[]=mob_calculator_non_taxable_amount&keys[]=mob_calculator_salary_tax&keys[]=mob_calculator_social_contributions_employee&keys[]=mob_calculator_social_contributions_employer";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    private void countB1N() {
        loadingLayout.setVisibility(View.VISIBLE);
        httpService = new VolleyService(this, this, REQUEST_TYPE_1);
        String params = "?keys[]=mob_calculator_max_base_social_contributions&keys[]=mob_calculator_non_taxable_amount&keys[]=mob_calculator_salary_tax&keys[]=mob_calculator_social_contributions_employee&keys[]=mob_calculator_social_contributions_employer";
        httpService.get(HTTP_CONFIG_URL + params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(CalculatorInputTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        httpService = new VolleyService(this, this, REQUEST_TYPE_SEARCH);
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        loadingLayout.setVisibility(View.GONE);
        switch (requestType) {
            case REQUEST_TYPE_SEARCH:
                responseSearch(res);
                break;
            case REQUEST_TYPE_1:
                responseB1N(res);
                break;
            case REQUEST_TYPE_2:
                responseB2N(res);
                break;
            case REQUEST_TYPE_3:
                responseNB1B2(res);
                break;
            case REQUEST_TYPE_4:
                responseNB(res);
                break;
            case REQUEST_TYPE_5:
                responseBN(res);
                break;
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        try {
            Toast.makeText(this, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void responseB1N(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            e11 = Double.parseDouble(entity.getString("mob_calculator_max_base_social_contributions"));
            e13 = Double.parseDouble(entity.getString("mob_calculator_non_taxable_amount"));
            d16 = Double.parseDouble(entity.getString("mob_calculator_salary_tax"));
            d18 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employee"));
            d20 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employer"));

            Locale currentLocale = Locale.getDefault();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);

            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            if (e10 > e11) {
                e12 = e11;
            } else {
                e12 = e10;
            }

            JSONArray results = new JSONArray();

            double d18 = this.d18 / 100;
            double d16 = this.d16 / 100;

            double result1 = e10 - (e10 - e13) * d16 - e12 * d18;
            double result2 = (e10 - e13) * d16;


            double result3 = e10 * d18;
            if (e10 > e12) {
                result3 = e12 * d18;
            }

            double d20 = this.d20 / 100;

            double result4 = e10 * d20;
            if (e10 > e12) {
                result4 = e12 * d20;
            }

            double result5 = e10 + result4;
            double result6 = (result2 + result3 + result4) / result1;
            result6 = result6 * 100;


            if (exchangeRate != 0) {
                result1 = result1 / exchangeRate;
            }
            result1 = Math.round(result1);

            JSONObject result1Object = new JSONObject();
            result1Object.put("key", getResources().getString(R.string.calculator_values_1));
            result1Object.put("value", decimalFormat.format(result1) + "");
            results.put(result1Object);

            if (exchangeRate != 0) {
                result2 = result2 / exchangeRate;
            }
            result2 = Math.round(result2);

            JSONObject result2Object = new JSONObject();
            result2Object.put("key", getResources().getString(R.string.calculator_values_2));
            result2Object.put("value", decimalFormat.format(result2) + "");

            results.put(result2Object);

            if (exchangeRate != 0) {
                result3 = result3 / exchangeRate;
            }
            result3 = Math.round(result3);

            JSONObject result3Object = new JSONObject();
            result3Object.put("key", getResources().getString(R.string.calculator_values_3));
            result3Object.put("value", decimalFormat.format(result3) + "");

            results.put(result3Object);

            if (exchangeRate != 0) {
                result4 = result4 / exchangeRate;
            }
            result4 = Math.round(result4);

            JSONObject result4Object = new JSONObject();
            result4Object.put("key", getResources().getString(R.string.calculator_values_4));
            result4Object.put("value", decimalFormat.format(result4) + "");
            results.put(result4Object);

            if (exchangeRate != 0) {
                result5 = result5 / exchangeRate;
            }
            result5 = Math.round(result5);

            JSONObject result5Object = new JSONObject();
            result5Object.put("key", getResources().getString(R.string.calculator_values_5));
            result5Object.put("value", decimalFormat.format(result5) + "");
            results.put(result5Object);

            decimalFormat = new DecimalFormat("####0.00");
            JSONObject result6Object = new JSONObject();
            result6Object.put("key", getResources().getString(R.string.calculator_values_6));
            result6Object.put("value", decimalFormat.format(result6) + "%");
            results.put(result6Object);

            Intent intent = new Intent(CalculatorInputTemplate.this, CalculatorResultTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("results", results.toString());
            intent.putExtra("option3", option3);
            intent.putExtra("activityName", activityName);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue", mobileInputValue);
            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void responseB2N(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            e12 = Double.parseDouble(entity.getString("mob_calculator_max_base_social_contributions"));
            e14 = Double.parseDouble(entity.getString("mob_calculator_non_taxable_amount"));
            d17 = Double.parseDouble(entity.getString("mob_calculator_salary_tax"));
            d19 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employee"));
            d21 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employer"));

            Locale currentLocale = Locale.getDefault();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);
            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            JSONArray results = new JSONArray();

            circularFormulaCounter = 0;
            calculateB2N(circularFormulaCounter);

            if (exchangeRate != 0) {
                e11 = e11 / exchangeRate;
            }
            JSONObject resultE11Object = new JSONObject();
            resultE11Object.put("key", getResources().getString(R.string.calculator_values_7));
            resultE11Object.put("value", decimalFormat.format(e11) + "");
            results.put(resultE11Object);

            if (exchangeRate != 0) {
                e15 = e15 / exchangeRate;
            }
            JSONObject resultE15Object = new JSONObject();
            resultE15Object.put("key", getResources().getString(R.string.calculator_values_1));
            resultE15Object.put("value", decimalFormat.format(e15) + "");
            results.put(resultE15Object);

            if (exchangeRate != 0) {
                e17 = e17 / exchangeRate;
            }
            JSONObject resultE17Object = new JSONObject();
            resultE17Object.put("key", getResources().getString(R.string.calculator_values_2));
            resultE17Object.put("value", decimalFormat.format(e17) + "");
            results.put(resultE17Object);

            if (exchangeRate != 0) {
                e19 = e19 / exchangeRate;
            }
            JSONObject resultE19Object = new JSONObject();
            resultE19Object.put("key", getResources().getString(R.string.calculator_values_3));
            resultE19Object.put("value", decimalFormat.format(e19) + "");
            results.put(resultE19Object);

            if (exchangeRate != 0) {
                e21 = e21 / exchangeRate;
            }
            JSONObject resultE21Object = new JSONObject();
            resultE21Object.put("key", getResources().getString(R.string.calculator_values_4));
            resultE21Object.put("value", decimalFormat.format(e21) + "");
            results.put(resultE21Object);

            decimalFormat = new DecimalFormat("####0.00");
            JSONObject resultE23Object = new JSONObject();
            resultE23Object.put("key", getResources().getString(R.string.calculator_values_6));
            resultE23Object.put("value", decimalFormat.format(e23) + "%");
            results.put(resultE23Object);

            Intent intent = new Intent(CalculatorInputTemplate.this, CalculatorResultTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("results", results.toString());
            intent.putExtra("option3", option3);
            intent.putExtra("activityName", activityName);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue", mobileInputValue);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calculateB2N(int counter) {
        e11 = e10 - e21;

        if (e11 > e12) {
            e13 = e12;
        } else {
            e13 = e11;
        }

        double d17 = this.d17 / 100;
        double d19 = this.d19 / 100;

        e15 = e11 - (e11 - e14) * d17 - (e13 * d19);
        e17 = (e11 - e14) * d17;

        if (e11 > e13) {
            e19 = e13 * d19;
        } else {
            e19 = e11 * d19;
        }

        double d21 = this.d21 / 100;
        if (e11 > e13) {
            e21 = e13 * d21;
        } else {
            e21 = e11 * d21;
        }

        e23 = ((e17 + e19 + e21) / e15) * 100;

        if (!((e11 == e15 + e17 + e19) && (e10 == e11 + e21) || counter > 100)) {
            counter++;
            calculateB2N(counter);
        }
    }

    private void responseNB1B2(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            e11 = Double.parseDouble(entity.getString("mob_calculator_max_base_social_contributions"));
            e13 = Double.parseDouble(entity.getString("mob_calculator_non_taxable_amount"));
            d17 = Double.parseDouble(entity.getString("mob_calculator_salary_tax"));
            d19 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employee"));
            d21 = Double.parseDouble(entity.getString("mob_calculator_social_contributions_employer"));
            Locale currentLocale = Locale.getDefault();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            JSONArray results = new JSONArray();

            circularFormulaCounter = 0;
            calculateNB1B2(circularFormulaCounter);

            if (exchangeRate != 0) {
                e14 = e14 / exchangeRate;
            }
            JSONObject resultE14Object = new JSONObject();
            resultE14Object.put("key", getResources().getString(R.string.calculator_values_7));
            resultE14Object.put("value", decimalFormat.format(e14) + "");
            results.put(resultE14Object);

            if (exchangeRate != 0) {
                e17 = e17 / exchangeRate;
            }
            JSONObject resultE17Object = new JSONObject();
            resultE17Object.put("key", getResources().getString(R.string.calculator_values_2));
            resultE17Object.put("value", decimalFormat.format(e17) + "");
            results.put(resultE17Object);

            if (exchangeRate != 0) {
                e19 = e19 / exchangeRate;
            }
            JSONObject resultE19Object = new JSONObject();
            resultE19Object.put("key", getResources().getString(R.string.calculator_values_3));
            resultE19Object.put("value", decimalFormat.format(e19) + "");
            results.put(resultE19Object);

            if (exchangeRate != 0) {
                e21 = e21 / exchangeRate;
            }
            JSONObject resultE21Object = new JSONObject();
            resultE21Object.put("key", getResources().getString(R.string.calculator_values_4));
            resultE21Object.put("value", decimalFormat.format(e21) + "");
            results.put(resultE21Object);

            if (exchangeRate != 0) {
                e23 = e23 / exchangeRate;
            }
            JSONObject resultE23Object = new JSONObject();
            resultE23Object.put("key", getResources().getString(R.string.calculator_values_5));
            resultE23Object.put("value", decimalFormat.format(e23) + "");
            results.put(resultE23Object);

            decimalFormat = new DecimalFormat("####0.00");
            JSONObject resultE25Object = new JSONObject();
            resultE25Object.put("key", getResources().getString(R.string.calculator_values_6));
            resultE25Object.put("value", decimalFormat.format(e25) + "%");
            results.put(resultE25Object);

            Intent intent = new Intent(CalculatorInputTemplate.this, CalculatorResultTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("results", results.toString());
            intent.putExtra("option3", option3);
            intent.putExtra("activityName", activityName);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue", mobileInputValue);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calculateNB1B2(int counter) {
        double d17 = this.d17 / 100;
        double d19 = this.d19 / 100;
        double d21 = this.d21 / 100;

        if (e14 > e11) {
            e14 = (e10 - (e13 * d17) + (e11 * d19)) / (1 - d17);
        } else {
            e14 = (e10 - (e13 * d17)) / (1 - d19 - d17);
        }

        e17 = (e14 - e13) * d17;

        if (e14 > e11) {
            e12 = e11;
        } else {
            e12 = e14;
        }

        if (e14 > e12) {
            e19 = e12 * d19;
        } else {
            e19 = e14 * d19;
        }

        if (e14 > e12) {
            e21 = e12 * d21;
        } else {
            e21 = e14 * d21;
        }

        e23 = e14 + e21;
        e25 = ((e17 + e19 + e21) / e10) * 100;

        if (!((e10 == e14 - e17 - e19) || counter > 100)) {
            counter++;
            calculateNB1B2(counter);
        }
    }

    private void responseNB(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            double i10 = Double.parseDouble(entity.getString("mob_calculator_standard_costs"));
            double i11 = Double.parseDouble(entity.getString("mob_calculator_personal_income_tax"));
            double i12 = Double.parseDouble(entity.getString("mob_calculator_contribution_to_pension_fund"));
            double i13 = Double.parseDouble(entity.getString("mob_calculator_contribution_to_health_fund"));
            Locale currentLocale = Locale.getDefault();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            double param1 = 1 - (i10 / 100);
            double param2 = i11 + i12;

            if (securityAnswer == NO_ANSWER) {
                param2 = i12 + i11 + i13;
            }

            double percentage2 = param1 * param2 / 100;
            double e11 = 1 / (1 - percentage2);

            double e12 = e10 * e11;
            double e13 = e12 * i10 / 100;
            double e14 = e12 - e13;
            double e16 = e14 * i11 / 100;
            double e18 = e14 * i12 / 100;
            double e19 = 0;

            if (securityAnswer == NO_ANSWER) {
                e19 = e14 * i13 / 100;
            }

            double e17 = e18 + e19;
            double e21 = e12;
            double e22 = (e16 + e17) / e10;
            e22 = e22 * 100;

            JSONArray results = new JSONArray();

            if (exchangeRate != 0) {
                e12 = e12 / exchangeRate;
            }
            JSONObject resultE12Object = new JSONObject();
            resultE12Object.put("key", getResources().getString(R.string.calculator_values_8));
            resultE12Object.put("value", decimalFormat.format(e12) + "");
            results.put(resultE12Object);

            if (exchangeRate != 0) {
                e16 = e16 / exchangeRate;
            }
            JSONObject resultE16Object = new JSONObject();
            resultE16Object.put("key", getResources().getString(R.string.calculator_values_9));
            resultE16Object.put("value", decimalFormat.format(e16) + "");
            results.put(resultE16Object);

            if (exchangeRate != 0) {
                e17 = e17 / exchangeRate;
            }
            JSONObject resultE17Object = new JSONObject();
            resultE17Object.put("key", getResources().getString(R.string.calculator_values_10));
            resultE17Object.put("value", decimalFormat.format(e17) + "");
            results.put(resultE17Object);

            if (exchangeRate != 0) {
                e21 = e21 / exchangeRate;
            }
            JSONObject resultE21Object = new JSONObject();
            resultE21Object.put("key", getResources().getString(R.string.calculator_values_11));
            resultE21Object.put("value", decimalFormat.format(e21) + "");
            results.put(resultE21Object);

            decimalFormat = new DecimalFormat("####0.00");
            JSONObject resultE22Object = new JSONObject();
            resultE22Object.put("key", getResources().getString(R.string.calculator_values_12));
            resultE22Object.put("value", decimalFormat.format(e22) + "%");
            results.put(resultE22Object);

            Intent intent = new Intent(CalculatorInputTemplate.this, CalculatorResultTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("results", results.toString());
            intent.putExtra("option3", option3);
            intent.putExtra("activityName", activityName);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue", mobileInputValue);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void responseBN(JSONObject res) {
        try {
            JSONObject entity = res.getJSONObject("entity");
            double i10 = Double.parseDouble(entity.getString("mob_calculator_standard_costs"));
            double i11 = Double.parseDouble(entity.getString("mob_calculator_personal_income_tax"));
            double i12 = Double.parseDouble(entity.getString("mob_calculator_contribution_to_pension_fund"));
            double i13 = Double.parseDouble(entity.getString("mob_calculator_contribution_to_health_fund"));
            Locale currentLocale = Locale.getDefault();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);            float exchangeRate = 0;
//            if (calculationValute.equals(CalculatorHelper.EUR_VALUE)) {
//                exchangeRate = Float.parseFloat(etExchangeRate.getText().toString());
//            }

            double e11 = e10 * i10 / 100;
            double e12 = e10 - e11;
            double e15 = e12 * i11 / 100;
            double e17 = e12 * i12 / 100;
            double e18 = e12 * i13 / 100;
            if (securityAnswer == YES_ANSWER) {
                e18 = 0;
            }
            double e16 = e17 + e18;
            double e13 = e10 - e15 - e16;
            double e20 = e10;
            double e21 = (e15 + e16) / e13;
            e21 = e21 * 100;

            JSONArray results = new JSONArray();

            if (exchangeRate != 0) {
                e13 = e13 / exchangeRate;
            }
            JSONObject resultE13Object = new JSONObject();
            resultE13Object.put("key", getResources().getString(R.string.calculator_values_13));
            resultE13Object.put("value", decimalFormat.format(e13) + "");
            results.put(resultE13Object);

            if (exchangeRate != 0) {
                e15 = e15 / exchangeRate;
            }
            JSONObject resultE15Object = new JSONObject();
            resultE15Object.put("key", getResources().getString(R.string.calculator_values_9));
            resultE15Object.put("value", decimalFormat.format(e15) + "");
            results.put(resultE15Object);

            if (exchangeRate != 0) {
                e16 = e16 / exchangeRate;
            }
            JSONObject resultE16Object = new JSONObject();
            resultE16Object.put("key", getResources().getString(R.string.calculator_values_10));
            resultE16Object.put("value", decimalFormat.format(e16) + "");
            results.put(resultE16Object);

            if (exchangeRate != 0) {
                e20 = e20 / exchangeRate;
            }
            JSONObject resultE20Object = new JSONObject();
            resultE20Object.put("key", getResources().getString(R.string.calculator_values_11));
            resultE20Object.put("value", decimalFormat.format(e20) + "");
            results.put(resultE20Object);

            decimalFormat = new DecimalFormat("####0.00");
            JSONObject resultE21Object = new JSONObject();
            resultE21Object.put("key", getResources().getString(R.string.calculator_values_12));
            resultE21Object.put("value", decimalFormat.format(e21) + "%");
            results.put(resultE21Object);

            Intent intent = new Intent(CalculatorInputTemplate.this, CalculatorResultTemplate.class);
            intent.putExtra("title", title);
            intent.putExtra("results", results.toString());
            intent.putExtra("option3", option3);
            intent.putExtra("activityName", activityName);
            intent.putExtra("calculation", calculation);
            intent.putExtra("calculationValute", option1);
            intent.putExtra("inputValue", mobileInputValue);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void responseSearch(JSONObject res) {
        try {
            JSONArray entity = res.getJSONArray("entity");

            for (int i = 0; i < entity.length(); i++) {
                JSONObject searchItem = entity.getJSONObject(i);
                createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
            }

            updateSearchListResultRecyclerView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
