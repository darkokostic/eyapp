package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.EYEventsTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.GridIconTemplate;
import rs.ey.taxserbia.activities.app.pages.types.GridSubHeaderBtnTemplate;
import rs.ey.taxserbia.activities.app.pages.types.IndustryOverviewTemplate;
import rs.ey.taxserbia.activities.app.pages.types.NewsListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.OnlyImagesTemplate;
import rs.ey.taxserbia.activities.app.pages.types.PollsListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.SwipedListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.VideoListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorEarningsTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorServiceContractTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorYearTaxTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplate;
import rs.ey.taxserbia.activities.app.pages.types.calendar.CalendarTemplateSecond;
import rs.ey.taxserbia.entities.pages.types.templates.GridIconItemTemplate;
import rs.ey.taxserbia.helpers.Utils;

import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_EARNINGS;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_SERVICE_CONTRACT;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALCULATOR_YEAR_TAX;
import static rs.ey.taxserbia.entities.pages.types.PagesType.CALENDAR_TEMPLATE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EVENT_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.EXPANDABLE_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.GRID_BG_CONTACT_TEMPLATE;
import static rs.ey.taxserbia.entities.pages.types.PagesType.GRID_ICON;
import static rs.ey.taxserbia.entities.pages.types.PagesType.INDUSTRY_OVERVIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.NEWS_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.PAGE_VIEW;
import static rs.ey.taxserbia.entities.pages.types.PagesType.POLL_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.SWIPED_LIST;
import static rs.ey.taxserbia.entities.pages.types.PagesType.VIDEO_LIST;

/**
 * Created by darko on 28.6.18..
 */

public class GridIconTemplateAdapter extends RecyclerView.Adapter<GridIconTemplateAdapter.GridIconTemplateViewHolder> {
    private ArrayList<GridIconItemTemplate> data;
    private LayoutInflater inflater;
    private Context context;

    public GridIconTemplateAdapter(Context context, ArrayList<GridIconItemTemplate> data) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public GridIconTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_grid_icon_template, parent, false);
        return new GridIconTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GridIconTemplateViewHolder holder, final int position) {
        final GridIconItemTemplate gridIconPageItem = data.get(position);
        Drawable defaultIcon = context.getResources().getDrawable(R.drawable.poreske_vesti_icon);
        String itemIconUrl = gridIconPageItem.getIconUrl();

        holder.txtGridIconPageItemTitle.setText(gridIconPageItem.getTitle());

        if (itemIconUrl == null || itemIconUrl.equals("null")) {
            holder.gridIconPageItemIcon.setImageDrawable(defaultIcon);
        } else {
            Utils.fetchSvg(context, itemIconUrl, holder.gridIconPageItemIcon);
        }

        holder.gridIconPageItemIcon.setVisibility(View.VISIBLE);

        holder.gridIconPageItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (gridIconPageItem.getType()) {
                    case GRID_ICON:
                        intent = new Intent(context, GridIconTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getId());
                        context.startActivity(intent);
                        break;
                    case NEWS_LIST:
                        intent = new Intent(context, NewsListTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getObjectId());
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case EXPANDABLE_LIST:
                        intent = new Intent(context, ExpandableListTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getObjectId());
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case VIDEO_LIST:
                        intent = new Intent(context, VideoListTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getObjectId());
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case POLL_LIST:
                        intent = new Intent(context, PollsListTemplate.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case EVENT_LIST:
                        intent = new Intent(context, EYEventsTemplate.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case SWIPED_LIST:
                        intent = new Intent(context, SwipedListTemplate.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case CALCULATOR_EARNINGS:
                        intent = new Intent(context, CalculatorEarningsTemplate.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case CALCULATOR_SERVICE_CONTRACT:
                        intent = new Intent(context, CalculatorServiceContractTemplate.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case CALCULATOR_YEAR_TAX:
                        intent = new Intent(context, CalculatorYearTaxTemplate.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case PAGE_VIEW:
                        intent = new Intent(context, OnlyImagesTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getObjectId());
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case CALENDAR_TEMPLATE:
                        intent = new Intent(context, CalendarTemplateSecond.class);
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case GRID_BG_CONTACT_TEMPLATE:
                        intent = new Intent(context, GridSubHeaderBtnTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getId());
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                    case INDUSTRY_OVERVIEW:
                        intent = new Intent(context, IndustryOverviewTemplate.class);
                        intent.putExtra("id", gridIconPageItem.getId());
                        intent.putExtra("title", gridIconPageItem.getTitle());
                        context.startActivity(intent);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class GridIconTemplateViewHolder extends RecyclerView.ViewHolder {
        TextView txtGridIconPageItemTitle;
        ImageView gridIconPageItemIcon;
        LinearLayout gridIconPageItemLayout;

        public GridIconTemplateViewHolder(View itemView) {
            super(itemView);
            txtGridIconPageItemTitle = itemView.findViewById(R.id.txt_grid_icon_item_title);
            gridIconPageItemIcon = itemView.findViewById(R.id.img_grid_icon_item_icon);
            gridIconPageItemLayout = itemView.findViewById(R.id.grid_icon_item_layout);
        }
    }
}
