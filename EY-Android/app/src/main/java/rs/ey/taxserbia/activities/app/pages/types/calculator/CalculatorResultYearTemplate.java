package rs.ey.taxserbia.activities.app.pages.types.calculator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.HomeActivity;
import rs.ey.taxserbia.activities.app.home.pages.SearchMoreActivity;
import rs.ey.taxserbia.activities.app.home.pages.SettingsActivity;
import rs.ey.taxserbia.activities.app.pages.types.AboutPageTemplate;
import rs.ey.taxserbia.activities.app.pages.types.ExpandableListTemplate;
import rs.ey.taxserbia.activities.app.pages.types.contact.ContactTemplate;
import rs.ey.taxserbia.adapters.HomeSearchResultListAdapter;
import rs.ey.taxserbia.entities.HomeSearchResult;
import rs.ey.taxserbia.helpers.CheckInternetConnection;
import rs.ey.taxserbia.helpers.LanguageHelper;
import rs.ey.taxserbia.helpers.ShareContent;
import rs.ey.taxserbia.services.http.Responsable;
import rs.ey.taxserbia.services.http.VolleyService;

import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B1_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.B2_VALUE;
import static rs.ey.taxserbia.activities.app.pages.types.calculator.CalculatorHelper.N_VALUE;

public class CalculatorResultYearTemplate extends AppCompatActivity implements Responsable {

    private final static String HTTP_SEARCH_URL = "https://ey.nbgcreator.com/api/news/search";
    private VolleyService httpService;

    private LinearLayout layoutSearch;
    private LinearLayout layoutSettings;
    private LinearLayout btnSettingsActive;
    private LinearLayout btnSearchClose;
    private LinearLayout layoutSettingsItemsBg;
    private ArrayList<HomeSearchResult> searchResultsList;
    private RecyclerView searchResultsListRecyclerView;
    private ImageView btnSearchMore;
    private EditText etSearch;
    private FrameLayout btnFaqSettings;
    private FrameLayout btnSettingsMenu;
    private FrameLayout btnAboutMenu;
    private ImageView btnBack;
    private ImageView imgLogo;

    private String title;
    private String calculation;
    private String calculationValute;
    private double inputValue1;
    private int inputValue2;
    private double result;

    private TextView txtCalculationValute;
    private TextView txtCalculation;
    private TextView txtOption1Label;
    private TextView txtOption3Label;
    private TextView txtInputValue1;
    private TextView txtInputValue2;
    private TextView txtResult;

    private ImageView btnShare;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_calculator_result_year);

        title = getIntent().getExtras().getString("title");
        calculation = getIntent().getExtras().getString("calculation");
        calculationValute = getIntent().getExtras().getString("calculationValute");
        inputValue1 = getIntent().getExtras().getDouble("inputValue1");
        inputValue2 = getIntent().getExtras().getInt("inputValue2");
        result = getIntent().getExtras().getDouble("result");

        StatusBarUtil.setColor(CalculatorResultYearTemplate.this, getResources().getColor(R.color.status_bar_intro));
        initComponents();
        initOnClickListeners();

        txtCalculationValute.setText(calculationValute);

        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);
        txtInputValue1.setText(decimalFormat.format(inputValue1) + "");
        txtInputValue2.setText(inputValue2 + "");
        txtResult.setText(decimalFormat.format(result) + "");

        switch (calculation) {
            case N_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_year_value1));
                break;
            case B1_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_year_value2));
                break;
            case B2_VALUE:
                txtCalculation.setText(getResources().getText(R.string.calc_billing_type_label) + " " + getResources().getString(R.string.calc_result_year_value3));
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(LanguageHelper.wrap(newBase, EyApplication.langShort));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    private void initComponents() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        btnBack = findViewById(R.id.btn_back);
        layoutSearch = findViewById(R.id.layout_search);
        layoutSettings = findViewById(R.id.layout_settings);
        btnSettingsActive = findViewById(R.id.btn_settings_active);
        btnSearchClose = findViewById(R.id.btn_close_search);
        layoutSettingsItemsBg = findViewById(R.id.layout_settings_items_bg);
        searchResultsListRecyclerView = findViewById(R.id.home_search_result_recyclerview);
        btnSearchMore = findViewById(R.id.btn_search_more);
        etSearch = findViewById(R.id.et_search);
        btnFaqSettings = findViewById(R.id.btn_faq_settings);
        btnSettingsMenu = findViewById(R.id.btn_settings_menu);
        btnAboutMenu = findViewById(R.id.btn_about_menu);
        imgLogo = findViewById(R.id.img_logo);

        txtCalculationValute = findViewById(R.id.txt_calculation_valute);
        txtCalculation = findViewById(R.id.txt_calculation);
        txtOption1Label = findViewById(R.id.txt_option_1_label);
        txtOption3Label = findViewById(R.id.txt_option_3_label);
        txtInputValue1 = findViewById(R.id.txt_input_value1);
        txtInputValue2 = findViewById(R.id.txt_input_value2);
        txtResult = findViewById(R.id.txt_result);

        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);

        httpService = new VolleyService(this, this);

        btnShare = findViewById(R.id.btn_share);
    }

    private void initOnClickListeners() {
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultYearTemplate.this, HomeActivity.class));
                finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSettingsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearch.setVisibility(View.GONE);
            }
        });

        layoutSettingsItemsBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSearchMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSearch.getText().toString().equals("")) {
                    layoutSearch.setVisibility(View.GONE);

                    Intent intent = new Intent(CalculatorResultYearTemplate.this, SearchMoreActivity.class);
                    intent.putExtra("query", etSearch.getText().toString());
                    startActivity(intent);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    String query = edit.toString().toLowerCase();
                    if (CheckInternetConnection.isOnline(getApplicationContext())) {
                        requestSearch(query);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    searchResultsList = new ArrayList<>();
                    updateSearchListResultRecyclerView();
                }
            }
        });

        btnFaqSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultYearTemplate.this, ExpandableListTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnSettingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultYearTemplate.this, SettingsActivity.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnAboutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculatorResultYearTemplate.this, AboutPageTemplate.class));
                layoutSettings.setVisibility(View.GONE);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locale currentLocale = Locale.getDefault();
                DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
                otherSymbols.setDecimalSeparator('.');
                otherSymbols.setGroupingSeparator('.');
                DecimalFormat decimalFormat = new DecimalFormat("#,###,###", otherSymbols);


                StringBuilder content = new StringBuilder();

                content.append(title);
                content.append("\n");
                content.append("\n");

                content.append(txtOption1Label.getText().toString());
                content.append("\n");
                content.append("\n");

                content.append(txtCalculationValute.getText().toString());
                content.append("\n");
                content.append("\n");

                content.append(txtCalculation.getText().toString());
                content.append("\n");
                content.append(inputValue1);
                content.append("\n");
                content.append("\n");

                content.append(txtOption3Label.getText().toString());
                content.append("\n");
                content.append(inputValue2);
                content.append("\n");
                content.append("\n");

                String result = getResources().getString(R.string.calc_result_year_result_label) + ": " + decimalFormat.format(CalculatorResultYearTemplate.this.result);
                content.append(result);
                ShareContent.share(CalculatorResultYearTemplate.this, content);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_action_search:
                layoutSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.home_action_contact:
                startActivity(new Intent(CalculatorResultYearTemplate.this, ContactTemplate.class));
                return true;
            case R.id.home_action_settings:
                layoutSettings.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSearchListResultRecyclerView() {
        searchResultsListRecyclerView.setHasFixedSize(true);
        final HomeSearchResultListAdapter searchResultAdapter = new HomeSearchResultListAdapter(getApplicationContext(), searchResultsList, HomeSearchResultListAdapter.HOME_SEARCH_LIST_LAYOUT);
        searchResultsListRecyclerView.setAdapter(searchResultAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        searchResultsListRecyclerView.setLayoutManager(layoutManager);
    }

    private void requestSearch(String query) {
        searchResultsList = new ArrayList<>();
        String params = "?lang=" + EyApplication.langShort
                + "&phrase=" + query;
        httpService.get(HTTP_SEARCH_URL + params);
    }

    @SuppressWarnings("SameParameterValue")
    private void createSearchResult(String id, String title, String desc) {
        HomeSearchResult searchResult = new HomeSearchResult(id, title, desc);
        searchResultsList.add(searchResult);
    }

    @Override
    public void onSuccessResponse(JSONObject res, int requestType) {
        try {
            JSONArray entity = res.getJSONArray("entity");

            for (int i = 0; i < entity.length(); i++) {
                JSONObject searchItem = entity.getJSONObject(i);
                createSearchResult(searchItem.getString("id"), searchItem.getString("cateory"), searchItem.getString("title"));
            }

            updateSearchListResultRecyclerView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(JSONObject err) {
        try {
            Toast.makeText(this, err.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
