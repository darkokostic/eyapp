package rs.ey.taxserbia.services.calendar;

/**
 * Created by darko on 20.11.18..
 */

public interface CalendarDateClickable {
    void onDateChanged(int dateNumber);
}
