package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.EYEventsSingleViewTemplate;
import rs.ey.taxserbia.entities.pages.types.templates.EYEventItem;

/**
 * Created by darko on 28.6.18..
 */

public class EYEventsListTemplateAdapter extends RecyclerView.Adapter<EYEventsListTemplateAdapter.EYEventsListTemplateViewHolder> {

    private ArrayList<EYEventItem> data;
    private LayoutInflater inflater;
    private String pageTitle;
    private Context context;
    private int viewType;
    public static final int VIEW_TYPE_ACTIVE = 1;
    public static final int VIEW_TYPE_PAST = 2;

    public EYEventsListTemplateAdapter(String pageTitle, Context context, ArrayList<EYEventItem> data, int viewType) {
        this.data = data;
        this.context = context;
        this.pageTitle = pageTitle;
        this.viewType = viewType;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public EYEventsListTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (this.viewType == VIEW_TYPE_ACTIVE) {
            view = inflater.inflate(R.layout.card_ey_events_list_template_active, parent, false);
        } else {
            view = inflater.inflate(R.layout.card_ey_events_list_template_past, parent, false);
        }
        return new EYEventsListTemplateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final EYEventsListTemplateViewHolder holder, final int position) {
        final EYEventItem eyEventItem = data.get(position);

        holder.txtDate.setText(eyEventItem.getStartDate());
        holder.txtTitle.setText(eyEventItem.getTitle());

        holder.layoutEYEventItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EYEventsSingleViewTemplate.class);
                intent.putExtra("id", eyEventItem.getId());
                intent.putExtra("title", pageTitle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class EYEventsListTemplateViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutEYEventItem;
        TextView txtDate;
        TextView txtTitle;

        public EYEventsListTemplateViewHolder(View itemView) {
            super(itemView);
            layoutEYEventItem = itemView.findViewById(R.id.layout_event_item);
            txtDate = itemView.findViewById(R.id.txt_date);
            txtTitle = itemView.findViewById(R.id.txt_title);
        }
    }
}
