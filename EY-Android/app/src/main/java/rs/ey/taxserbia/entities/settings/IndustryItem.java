package rs.ey.taxserbia.entities.settings;

/**
 * Created by darko on 9.9.18..
 */

public class IndustryItem {

    private String id;
    private String name;

    public IndustryItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
