package rs.ey.taxserbia.adapters.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.EyApplication;
import rs.ey.taxserbia.R;
import rs.ey.taxserbia.entities.settings.IndustryItem;
import rs.ey.taxserbia.services.LocalStorage;

/**
 * Created by darko on 9.9.18..
 */

public class IndustriesListAdapter extends RecyclerView.Adapter<IndustriesListAdapter.IndustriesListViewHolder> {

    private ArrayList<IndustryItem> data;
    private LayoutInflater inflater;
    private Context context;
    private FrameLayout cbAll;
    private ImageView imgCbAll;

    public IndustriesListAdapter(Context context, ArrayList<IndustryItem> data, FrameLayout cbAll, ImageView imgCbAll) {
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.cbAll = cbAll;
        this.imgCbAll = imgCbAll;
    }

    @Override
    public IndustriesListAdapter.IndustriesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_choose_industries_settings, parent, false);
        return new IndustriesListAdapter.IndustriesListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final IndustriesListAdapter.IndustriesListViewHolder holder, final int position) {
        final IndustryItem industryItem = data.get(position);

        holder.txtName.setText(industryItem.getName());
        holder.imgCheckbox.setVisibility(View.GONE);

        for (IndustryItem item : EyApplication.chosenIndustries) {
            if (item.getId().equals(industryItem.getId())) {
                holder.imgCheckbox.setVisibility(View.VISIBLE);
                holder.industriesItemLayout.setBackgroundColor(context.getResources()
                        .getColor(R.color.intro_step_4_active_cb_bg));
            }
        }

        holder.industriesItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                for (IndustryItem item : EyApplication.chosenIndustries) {
                    if (item.getId().equals(industryItem.getId())) {
                        count++;
                    }
                }

                if (count == 0) {
                    EyApplication.chosenIndustries.add(industryItem);
                    holder.imgCheckbox.setVisibility(View.VISIBLE);
                    holder.industriesItemLayout.setBackgroundColor(context.getResources()
                            .getColor(R.color.intro_step_4_active_cb_bg));
                } else {
                    for (IndustryItem item : EyApplication.chosenIndustries) {
                        if (item.getId().equals(industryItem.getId())) {
                            EyApplication.chosenIndustries.remove(item);
                            break;
                        }
                    }
                    holder.imgCheckbox.setVisibility(View.GONE);
                    holder.industriesItemLayout.setBackgroundColor(context.getResources()
                            .getColor(R.color.intro_step_4_inactive_cb_bg));

                    EyApplication.cbAllStatus = false;
                    LocalStorage.setIsSelectedAll(context, false);
                    imgCbAll.setVisibility(View.GONE);
                    cbAll.setBackgroundColor(context.getResources().getColor(R.color.intro_step_4_inactive_cb_bg));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class IndustriesListViewHolder extends RecyclerView.ViewHolder {
        FrameLayout industriesItemLayout;
        ImageView imgCheckbox;
        TextView txtName;

        public IndustriesListViewHolder(View itemView) {
            super(itemView);
            industriesItemLayout = itemView.findViewById(R.id.layout_card_industry);
            imgCheckbox = itemView.findViewById(R.id.img_checkbox);
            txtName = itemView.findViewById(R.id.txt_name);
        }
    }
}
