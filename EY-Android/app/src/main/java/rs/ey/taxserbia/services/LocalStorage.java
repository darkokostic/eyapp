package rs.ey.taxserbia.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import rs.ey.taxserbia.entities.settings.IndustryItem;

/**
 * Created by darko on 13.9.18..
 */

@SuppressWarnings("UnnecessaryLocalVariable")
public class LocalStorage {

    public static void setChosenIndustries(Context context, List<IndustryItem> chosenIndustries) {
        Gson gson = new Gson();
        String chosenIndustriesJson = gson.toJson(chosenIndustries);
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localStoragePreference.edit();
        edit.putString("chosenIndustries", chosenIndustriesJson);
        edit.apply();
    }

    public static List<IndustryItem> getChosenIndustries(Context context) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<IndustryItem>>(){}.getType();
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);

        String chosenIndustriesJson = localStoragePreference.getString("chosenIndustries", "");
        List<IndustryItem> chosenIndustries = gson.fromJson(chosenIndustriesJson, type);
        return chosenIndustries;
    }

    public static void setLanguageLong(Context context, String language) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localStoragePreference.edit();
        edit.putString("languageLong", language);
        edit.apply();
    }

    public static String getLanguageLong(Context context) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        return localStoragePreference.getString("languageLong", "");
    }

    public static void setLanguageShort(Context context, String language) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localStoragePreference.edit();
        edit.putString("languageShort", language);
        edit.apply();
    }

    public static String getLanguageShort(Context context) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        return localStoragePreference.getString("languageShort", "");
    }

    public static void setPushNotificationsStatus(Context context, boolean status) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localStoragePreference.edit();
        edit.putBoolean("pushStatus", status);
        edit.apply();
    }

    public static boolean getPushNotificationsStatus(Context context) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        return localStoragePreference.getBoolean("pushStatus", true);
    }

    public static void setIsSelectedAll(Context context, boolean isSelectedAll) {
        String isSelectedAllString = "false";
        if (isSelectedAll) isSelectedAllString = "true";

        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localStoragePreference.edit();
        edit.putString("selectedAll", isSelectedAllString);
        edit.apply();
    }

    public static boolean getIsSelectedAll(Context context) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        String selectedAllString = localStoragePreference.getString("selectedAll", "");
        boolean selectedAll = true;
        if (selectedAllString.equals("false")) {
            selectedAll = false;
        }
        return selectedAll;
    }

    public static void setHasIntro(Context context, boolean hasIntro) {
        String hasIntroString = "false";
        if (hasIntro) hasIntroString = "true";

        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localStoragePreference.edit();
        edit.putString("hasIntro", hasIntroString);
        edit.apply();
    }

    public static boolean getHasIntro(Context context) {
        SharedPreferences localStoragePreference = context.getSharedPreferences("localStoragePreference", Context.MODE_PRIVATE);
        String hasIntroString = localStoragePreference.getString("hasIntro", "");
        boolean hasIntro = true;
        if (hasIntroString.equals("false")) {
            hasIntro = false;
        }
        return hasIntro;
    }
}
