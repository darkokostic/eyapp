package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 28.6.18..
 */

public class GridBgPageTemplate {
    private String id;
    private String objectId;
    private String title;
    private String imageUrl;
    private String type;

    public GridBgPageTemplate(String id, String objectId, String title, String imageUrl, String type) {
        this.id = id;
        this.objectId = objectId;
        this.title = title;
        this.imageUrl = imageUrl;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
