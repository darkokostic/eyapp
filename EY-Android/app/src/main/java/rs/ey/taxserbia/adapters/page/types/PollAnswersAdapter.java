package rs.ey.taxserbia.adapters.page.types;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.entities.pages.types.templates.PollAnswerItem;

/**
 * Created by darko on 28.6.18..
 */

public class PollAnswersAdapter extends RecyclerView.Adapter<PollAnswersAdapter.PollAnswersViewHolder> {

    private ArrayList<PollAnswerItem> data;
    private LayoutInflater inflater;
    private Context context;
    private PollAnswerItemClickHandler answerClickHandler;

    public PollAnswersAdapter(Context context, ArrayList<PollAnswerItem> data, PollAnswerItemClickHandler answerClickHandler) {
        this.data = data;
        this.context = context;
        this.answerClickHandler = answerClickHandler;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public PollAnswersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_answer_item_poll_list_template, parent, false);
        return new PollAnswersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PollAnswersViewHolder holder, final int position) {
        final PollAnswerItem pollAnswerItem = data.get(position);

        holder.txtAnswerOption.setText(pollAnswerItem.getOption());
        holder.txtOptionCount.setText(pollAnswerItem.getAnswerCountChar());
        holder.layoutPollAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answerClickHandler.openPollAnswerClick(pollAnswerItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PollAnswersViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutPollAnswer;
        TextView txtAnswerOption;
        TextView txtOptionCount;

        public PollAnswersViewHolder(View itemView) {
            super(itemView);
            layoutPollAnswer = itemView.findViewById(R.id.layout_poll_answer);
            txtAnswerOption = itemView.findViewById(R.id.txt_answer_option);
            txtOptionCount = itemView.findViewById(R.id.txt_checkbox_char);
        }
    }
}
