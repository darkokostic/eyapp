package rs.ey.taxserbia.entities;

/**
 * Created by darko on 30.6.18..
 */

public class HomeSearchResult {
    private String id;
    private String title;
    private String desc;

    public HomeSearchResult(String id, String title, String desc) {
        this.id = id;
        this.title = title;
        this.desc = desc;
    }

    public HomeSearchResult(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
