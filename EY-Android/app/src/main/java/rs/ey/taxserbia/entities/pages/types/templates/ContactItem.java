package rs.ey.taxserbia.entities.pages.types.templates;

/**
 * Created by darko on 31.7.18..
 */

public class ContactItem {
    private String id;
    private String name;
    private String position;
    private String thumbnailUrl;

    public ContactItem(String id, String name, String position, String thumbnailUrl) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }


    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
