package rs.ey.taxserbia.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ey.taxserbia.R;
import rs.ey.taxserbia.activities.app.pages.types.NewsListSingleViewTemplate;
import rs.ey.taxserbia.entities.HomeSearchResult;

/**
 * Created by darko on 28.6.18..
 */

public class HomeSearchResultListAdapter extends RecyclerView.Adapter<HomeSearchResultListAdapter.HomeSearchResultListViewHolder> {

    public static final int HOME_SEARCH_LIST_LAYOUT = 1;
    public static final int HOME_SEARCH_MORE_LIST_LAYOUT = 2;
    private int layoutType;
    private ArrayList<HomeSearchResult> data;
    private LayoutInflater inflater;
    private Context context;

    public HomeSearchResultListAdapter(Context context, ArrayList<HomeSearchResult> data, int layoutType) {
        this.data = data;
        this.layoutType = layoutType;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public HomeSearchResultListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (layoutType == HOME_SEARCH_LIST_LAYOUT) {
            view = inflater.inflate(R.layout.home_search_result_item, parent, false);
        } else {
            view = inflater.inflate(R.layout.home_search_more_result_item, parent, false);
        }

        return new HomeSearchResultListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeSearchResultListViewHolder holder, final int position) {
        final HomeSearchResult searchResult = data.get(position);
        holder.txtTitle.setText(searchResult.getTitle());
        holder.txtDesc.setText(searchResult.getDesc());
        holder.layoutSearchItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewsListSingleViewTemplate.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", searchResult.getId());
                intent.putExtra("title", searchResult.getDesc());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class HomeSearchResultListViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtDesc;
        LinearLayout layoutSearchItem;

        public HomeSearchResultListViewHolder(View itemView) {
            super(itemView);
            layoutSearchItem = itemView.findViewById(R.id.layout_search_item);
            txtTitle = itemView.findViewById(R.id.txt_search_item_title);
            txtDesc = itemView.findViewById(R.id.txt_search_item_desc);
        }
    }
}
